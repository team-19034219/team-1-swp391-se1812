USE [master]
GO

/*******************************************************************************
   Drop database if it exists
********************************************************************************/
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'SWP1')
BEGIN
	ALTER DATABASE SWP1 SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE SWP1 SET ONLINE;
	DROP DATABASE SWP1;
END

GO

CREATE DATABASE SWP1
GO

USE SWP1
GO

/*******************************************************************************
	Drop tables if exists
*******************************************************************************/
DECLARE @sql nvarchar(MAX) 
SET @sql = N'' 

SELECT @sql = @sql + N'ALTER TABLE ' + QUOTENAME(KCU1.TABLE_SCHEMA) 
    + N'.' + QUOTENAME(KCU1.TABLE_NAME) 
    + N' DROP CONSTRAINT ' -- + QUOTENAME(rc.CONSTRAINT_SCHEMA)  + N'.'  -- not in MS-SQL
    + QUOTENAME(rc.CONSTRAINT_NAME) + N'; ' + CHAR(13) + CHAR(10) 
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
    ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
    AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
    AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

EXECUTE(@sql) 

GO
DECLARE @sql2 NVARCHAR(max)=''

SELECT @sql2 += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
FROM   INFORMATION_SCHEMA.TABLES
WHERE  TABLE_TYPE = 'BASE TABLE'

Exec Sp_executesql @sql2 
GO 

---------- CREATE TABLE -----------------------------

CREATE TABLE Roles(
	ID int primary key not null,
	name nvarchar(20)
);

CREATE TABLE Admins (
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    role_ID int,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)

);

CREATE TABLE Students(
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    role_ID int,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
	status int,
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)
);

CREATE TABLE Manager(
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    role_ID int,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)
);


CREATE TABLE Lectures (
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    role_ID int,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
	img_certificates nvarchar(50),
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)
);

CREATE TABLE TeachingAssistant(
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    role_ID int,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
	img_certificates nvarchar(50),
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)
);

CREATE TABLE CategoryOfCourse (
    id INT identity(1,1) PRIMARY KEY,
    name NVARCHAR(50), 
);

CREATE TABLE CategoryOfBlog (
    id INT identity(1,1) PRIMARY KEY,
    name NVARCHAR(50), 
);

CREATE TABLE Course(
    id int identity(1,1) PRIMARY KEY NOT NULL,
	name NVARCHAR(50) NOT NULL,
	price FLOAT,
    date Date NOT NULL,
	CategoryOfCourse_ID INT,
	Lecture_ID INT,
	discount float,
	img nvarchar(100),
	video nvarchar(50),
	document_link nvarchar(50),
	description nvarchar(500),
    FOREIGN KEY (CategoryOfCourse_ID) REFERENCES CategoryOfCourse(id),
	FOREIGN KEY (Lecture_ID) REFERENCES Lectures(id)
);

CREATE TABLE Progress(
	Student_ID int,
	Course_ID int,
	Lecture_ID int,
	status int,
	FOREIGN KEY (Student_ID) REFERENCES Students(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Lecture_ID) REFERENCES Lectures(id)
);


CREATE TABLE Orders(
	ID int identity(1,1) PRIMARY KEY,
	Student_ID int,
	Course_ID int,
	date varchar(40),
	description nvarchar(100),
	number_of_course int,
	total float,
	status int,
	FOREIGN KEY (Student_ID) REFERENCES Students(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
);


CREATE TABLE Payments(
	ID int identity(1,1) PRIMARY KEY,
	Course_ID int,
	Order_ID int,
	content_Payment nvarchar(50),
	quantity int,
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Order_ID) REFERENCES Orders(id),
);

CREATE TABLE Feedback(
	rate int,
	content nvarchar(300),
	daycomment date,
	Student_ID int,
	Course_ID int,
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Student_ID) REFERENCES Students(id)
);

CREATE TABLE Questions(
	id int primary key not null,
	Lecture_ID int,
	TeachingAssistant_ID int,
	Student_ID int,
	Course_ID int,
	content_of_question nvarchar(500),
	day_ask date
	FOREIGN KEY (Lecture_ID) REFERENCES Lectures(id),
	FOREIGN KEY (TeachingAssistant_ID) REFERENCES TeachingAssistant(id),
	FOREIGN KEY (Student_ID) REFERENCES Students(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id)
);

CREATE TABLE AnswerQuestion(
	Question_ID int,
	Lecture_ID int,
	TeachingAssistant_ID int,
	Student_ID int,
	Course_ID int,
	content_of_answer nvarchar(500),
	day_answer date,
	FOREIGN KEY (Question_ID) REFERENCES Questions(id),
	FOREIGN KEY (Lecture_ID) REFERENCES Lectures(id),
	FOREIGN KEY (TeachingAssistant_ID) REFERENCES TeachingAssistant(id),
	FOREIGN KEY (Student_ID) REFERENCES Students(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id)
);

CREATE TABLE Blog(
	ID int identity(1,1) PRIMARY KEY NOT NULL,
	Manager_ID int,
	Course_ID int,
	CategoryOfBlog_ID int,
	title nvarchar(100),
	content nvarchar(500),
	daypost date,
	tag nvarchar(100),
	number_of_like int
	FOREIGN KEY (Manager_ID) REFERENCES Manager(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (CategoryOfBlog_ID) REFERENCES CategoryOfBlog(id)
);

CREATE TABLE Comments(
	content nvarchar(500),
	Blog_ID int,
	Student_ID int,
	day_comment date,
	number_of_like int,
	FOREIGN KEY (Blog_ID) REFERENCES Blog(id),
	FOREIGN KEY (Student_ID) REFERENCES Students(id)
);

CREATE TABLE Certificates(
	Student_ID int,
	Course_ID int,
	img_certificate nvarchar(50),
	date_of_certificate date,
	FOREIGN KEY (Student_ID) REFERENCES Students(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
);
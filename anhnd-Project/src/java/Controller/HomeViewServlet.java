package Controller;

import DAL.BlogDAO;
import DAL.CategoryDAO;
import DAL.CourseDAO;
import DAL.LecturersDAO;
import Model.Blog;
import Model.CategoryOfCourse;
import Model.Course;
import Model.Lecturers;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(name = "HomeViewServlet", urlPatterns = {"/home"})
public class HomeViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO courseDAO = new CourseDAO();
        LecturersDAO ld = new LecturersDAO();
        CategoryDAO cd = new CategoryDAO();
        BlogDAO bd = new BlogDAO();

        List<Blog> banner = bd.getHome(5);
        List<Blog> slogan = bd.getHome(6);
        List<Blog> header = bd.getHome(8);
        Blog logo = bd.getBlog(9);
        // Lấy danh sách các khóa học và giảng viên
        List<Course> courses = courseDAO.getAll();

        List<Lecturers> lectures = ld.getAllLectures();

        List<CategoryOfCourse> cate = cd.getAllCategory();

        // Đặt danh sách vào thuộc tính của yêu cầu
        request.setAttribute("banner", banner);
        request.setAttribute("slogan", slogan);
        request.setAttribute("header", header);
        request.setAttribute("logo", logo);
        request.setAttribute("courses", courses);
        request.setAttribute("cate", cate);
        request.setAttribute("lectures", lectures);

        // Chuyển hướng đến trang home.jsp
        request.getRequestDispatcher("homepage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAL.BlogDAO;
import DAL.CategoryOfBlogDAO;
import DAL.CourseDAO;
import Model.Blog;
import Model.CategoryOfBlog;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nduya
 */
@WebServlet(name = "ManageSettingServlet", urlPatterns = {"/managesetting"})
public class ManageSettingServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageSettingServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageSettingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CategoryOfBlogDAO cobDAO = new CategoryOfBlogDAO();
        BlogDAO blogDAO = new BlogDAO();

        // Get parameters from request
        String cobIDParam = request.getParameter("cobID");
        String statusParam = request.getParameter("status");
        String blogIDParam = request.getParameter("blogID");

        // Initialize variables
        int cobID = 0;
        int status = 0;
        int blogID = 0;

        // Parse parameters if not null
        if (cobIDParam != null) {
            cobID = Integer.parseInt(cobIDParam);
        }
        if (statusParam != null && blogIDParam != null) {
            status = Integer.parseInt(statusParam);
            blogID = Integer.parseInt(blogIDParam);
        }

        // Update blog status if both status and blogID are provided
        if (statusParam != null && blogIDParam != null) {
            blogDAO.editStatusByID(blogID, status);
        }

        // Get list of categories and blogs
        List<CategoryOfBlog> listcob = cobDAO.getAll();
        List<Blog> listblog = (cobID != 0) ? blogDAO.getHome(cobID) : blogDAO.getAll();

        //status
        String st = request.getParameter("statusOrder");
        int statusOrder = -1;
        if (st != null) {
            statusOrder = Integer.parseInt(st);
            listblog = blogDAO.getBlogByStatus(statusOrder);
        }

        int page = 1;
        String productsPerPageStr = request.getParameter("productsPerPage");
        int productsPerPage;

        if (productsPerPageStr == null) {
            Integer sessionProductsPerPage = (Integer) session.getAttribute("numberpage");
            if (sessionProductsPerPage == null) {
                productsPerPage = 8; // Default value
                session.setAttribute("numberpage", productsPerPage);
            } else {
                productsPerPage = sessionProductsPerPage;
            }
        } else {
            productsPerPage = Integer.parseInt(productsPerPageStr);
            session.setAttribute("numberpage", productsPerPage);
        }

        int size = listblog.size();
        int totalPages = (size % productsPerPage == 0 ? (size / productsPerPage) : (size / productsPerPage) + 1);
        String pageStr = request.getParameter("page");
        if (pageStr != null && !pageStr.isEmpty()) {
            page = Integer.parseInt(pageStr);
            if (page < 1) {
                page = 1;
            } else if (page > totalPages) {
                page = totalPages;
            }
        }

        int start = (page - 1) * productsPerPage;
        int end = Math.min(page * productsPerPage, size);
        listblog = blogDAO.sophantutren1trang(listblog, start, end);

        // Set attributes and forward request
        session.setAttribute("listblog", listblog);
        request.setAttribute("listcob", listcob);
        request.setAttribute("page", page);
        session.setAttribute("statusOrder", statusOrder);
        request.setAttribute("num", totalPages);
        request.setAttribute("cobId", cobID);
        request.getRequestDispatcher("managesetting.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

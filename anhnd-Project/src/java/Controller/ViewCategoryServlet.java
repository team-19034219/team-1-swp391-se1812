/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAL.BlogDAO;
import DAL.CategoryOfBlogDAO;
import DAL.CategoryOfCourseDAO;
import DAL.CourseDAO;
import Model.Blog;
import Model.Category;
import Model.CategoryOfBlog;
import Model.Course;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author nduya
 */
@WebServlet(name = "ViewCategoryServlet", urlPatterns = {"/viewcategory"})
public class ViewCategoryServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CategoryOfCourseDAO cocDAO = new CategoryOfCourseDAO();
        CategoryOfBlogDAO cobDAO = new CategoryOfBlogDAO();
        CourseDAO cDAO = new CourseDAO();
        List<Category> listcategory = cocDAO.getAll();
        List<Course> listcourse = cDAO.getAll();
        BlogDAO bd = new BlogDAO();
        List<Blog> header = bd.getHome(8);
        List<Blog> orderBy = bd.getHome(10);
        Blog logo = bd.getBlog(9);
        CategoryOfBlog getOrder = cobDAO.getCategoryOfBlogByID(10);
        String priceFrom = request.getParameter("pricefrom");
        String priceFrom_raw = extractNumber(priceFrom);
        String priceTo = request.getParameter("priceto");
        String priceTo_raw = extractNumber(priceTo);
        Boolean[] chid = new Boolean[listcategory.size() + 1];
        Boolean[] chid2 = new Boolean[]{false, false, false, false, false};

        //RefineBrand
        String[] cid_refinee_raw = request.getParameterValues("cid_refinee");
        int[] cid_refinee = null;

        //RefinePrice
        int price1 = ((priceFrom_raw == null || "".equals(priceFrom_raw)) ? cDAO.getMinPrice() : Integer.parseInt(priceFrom_raw));
        int price2 = ((priceTo_raw == null || "".equals(priceTo_raw)) ? cDAO.getMaxPrice() : Integer.parseInt(priceTo_raw));

        //discount
        String[] discounts = request.getParameterValues("discount");
        double[] discountValue = (discounts == null ? null : new double[discounts.length]);

        // Search by both price and name if price and name are specified
        String txtSearch = request.getParameter("txt");

        //orderby
        String order = request.getParameter("orderBy");

        // Use session attributes if request parameters are not present
        if (priceFrom_raw == null) {
            priceFrom_raw = (String) session.getAttribute("pricefrom");
        } else {
            session.setAttribute("pricefrom", priceFrom_raw);
        }
        if (priceTo_raw == null) {
            priceTo_raw = (String) session.getAttribute("priceto");
        } else {
            session.setAttribute("priceto", priceTo_raw);
        }
        if (txtSearch == null) {
            txtSearch = (String) session.getAttribute("txtSearch");
        } else {
            session.setAttribute("txtSearch", txtSearch);
        }
        if (order == null) {
            order = (String) session.getAttribute("order");
        } else {
            session.setAttribute("order", order);
        }
        if (cid_refinee_raw == null) {
            cid_refinee_raw = (String[]) session.getAttribute("cid_refinee");
        } else {
            session.setAttribute("cid_refinee", cid_refinee_raw);
        }
        if (discounts == null) {
            discounts = (String[]) session.getAttribute("discounts");
        } else {
            session.setAttribute("discounts", discounts);
        }

        //discount
        if (discounts == null) {
            chid2[0] = true;
        } else {
            chid2[0] = false;
        }
        // Parse the discount values if provided
        if (discounts != null) {
            discountValue = new double[discounts.length];
            for (int i = 0; i < discounts.length; i++) {
                discountValue[i] = Double.parseDouble(discounts[i]);
                // Set chid2 based on the discounts provided
                if (discountValue[i] == 0.5) {
                    chid2[1] = true;
                }
                if (discountValue[i] == 0.3) {
                    chid2[2] = true;
                }
                if (discountValue[i] == 0.2) {
                    chid2[3] = true;
                }
                if (discountValue[i] == 0.1) {
                    chid2[4] = true;
                }
                if (discountValue[i] == 1) {
                    chid2[0] = true;
                    chid2[1] = false;
                    chid2[2] = false;
                    chid2[3] = false;
                    chid2[4] = false;
                }
            }
        }

        // Parse the category IDs if provided
        if (cid_refinee_raw != null) {
            cid_refinee = new int[cid_refinee_raw.length];
            for (int i = 0; i < cid_refinee.length; i++) {
                cid_refinee[i] = Integer.parseInt(cid_refinee_raw[i]);
            }
        }

        listcourse = cDAO.searchALL(cid_refinee, price1, price2, txtSearch, discountValue, order);

        //RefineBrand
        if (cid_refinee_raw == null) {
            chid[0] = true;
        } else {
            chid[0] = false;
        }
        //RefineBrand
        if ((cid_refinee_raw != null) && (cid_refinee[0] != 0)) {
            chid[0] = false;
            for (int i = 1; i < chid.length; i++) {
                if (isCheck(listcategory.get(i - 1).getId(), cid_refinee)) {
                    chid[i] = true;
                } else {
                    chid[i] = false;
                }
            }
        } else {
            chid[0] = true;
        }

        double minprice = cDAO.getMinPrice();
        double maxprice = cDAO.getMaxPrice();

        int page = 1;
        String productsPerPageStr = request.getParameter("productsPerPage");
        int productsPerPage;

        if (productsPerPageStr == null) {
            Integer sessionProductsPerPage = (Integer) session.getAttribute("numberpage");
            if (sessionProductsPerPage == null) {
                productsPerPage = 8; // Default value
                session.setAttribute("numberpage", productsPerPage);
            } else {
                productsPerPage = sessionProductsPerPage;
            }
        } else {
            productsPerPage = Integer.parseInt(productsPerPageStr);
            session.setAttribute("numberpage", productsPerPage);
        }

        int size = listcourse.size();
        int totalPages = (size % productsPerPage == 0 ? (size / productsPerPage) : (size / productsPerPage) + 1);
        String pageStr = request.getParameter("page");
        if (pageStr != null && !pageStr.isEmpty()) {
            page = Integer.parseInt(pageStr);
            if (page < 1) {
                page = 1;
            } else if (page > totalPages) {
                page = totalPages;
            }
        }

        int start = (page - 1) * productsPerPage;
        int end = Math.min(page * productsPerPage, size);
        listcourse = cDAO.sophantutren1trang(listcourse, start, end);

        request.setAttribute("minprice", minprice);
        request.setAttribute("maxprice", maxprice);
        request.setAttribute("sprice", price1);
        request.setAttribute("eprice", price2);
        request.setAttribute("chid", chid);
        request.setAttribute("chid2", chid2);
        request.setAttribute("header", header);
        request.setAttribute("listOrder", orderBy);
        request.setAttribute("getorder", getOrder);
        request.setAttribute("logo", logo);
        request.setAttribute("txtSearch", txtSearch);
        request.setAttribute("listcategory", listcategory);
        request.setAttribute("listcourse", listcourse);
        request.setAttribute("page", page);
        request.setAttribute("num", totalPages);
        request.getRequestDispatcher("viewbycategory.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
    //RefineBrand

    private boolean isCheck(int d, int[] id) {
        if (id == null) {
            return false;
        } else {
            for (int i = 0; i < id.length; i++) {
                if (id[i] == d) {
                    return true;
                }
            }
        }
        return false;
    }

    private String extractNumber(String str) {
        if (str != null) {
            // Loại bỏ tất cả các kí tự không phải là số từ chuỗi
            String cleanedStr = str.replaceAll("[^0-9]", "");
            return cleanedStr;
        }
        return ""; // Trả về giá trị mặc định nếu không có số nào hoặc có lỗi xảy ra
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

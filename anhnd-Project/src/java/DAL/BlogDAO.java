package DAL;

import Model.Blog;
import Model.CategoryOfBlog;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

public class BlogDAO extends DBContext {

    CategoryOfBlogDAO cobDAO = new CategoryOfBlogDAO();

    public List<Blog> getAll() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM Blog";
        try {

            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                int managerId = rs.getInt("Manager_ID");
                int courseId = rs.getInt("Course_ID");
                int cob_id = rs.getInt("CategoryOfBlog_ID");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String imgOfBlog = rs.getString("img_of_blog");
                Date dayPost = rs.getDate("daypost");
                String tag = rs.getString("tag");
                int numberOfLike = rs.getInt("number_of_like");
                int status = rs.getInt("status");
                CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(cob_id);

                Blog blog = new Blog(id, managerId, courseId, cob, title, content, imgOfBlog, dayPost, tag, numberOfLike, status);
                list.add(blog);
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return list;
    }

    public List<Blog> getHome(int CategoryOfBlog_ID) {
        //banner 5
        // sologan 6
        // about us 7
        //header 8
        //order 10
        //footer left 11
        //support 12
        //contact 13
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM Blog where CategoryOfBlog_ID = ?";
        try {

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, CategoryOfBlog_ID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                int managerId = rs.getInt("Manager_ID");
                int courseId = rs.getInt("Course_ID");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String imgOfBlog = rs.getString("img_of_blog");
                Date dayPost = rs.getDate("daypost");
                String tag = rs.getString("tag");
                int numberOfLike = rs.getInt("number_of_like");
                int status = rs.getInt("status");
                CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(CategoryOfBlog_ID);

                Blog blog = new Blog(id, managerId, courseId, cob, title, content, imgOfBlog, dayPost, tag, numberOfLike, status);
                list.add(blog);
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return list;
    }

    public List<Blog> getBlogByStatus(int status) {
        List<Blog> list = new ArrayList<>();
        String sql = "";
        if (status != 1 && status != 0) {
            sql = "SELECT * FROM Blog";
        } else {
            sql = "SELECT * FROM Blog where status = ?";
        }
        try {

            PreparedStatement stmt = connection.prepareStatement(sql);
            if (status == 1 || status == 0) {
                stmt.setInt(1, status);
            }
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                int managerId = rs.getInt("Manager_ID");
                int courseId = rs.getInt("Course_ID");
                int cobid = rs.getInt("CategoryOfBlog_ID");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String imgOfBlog = rs.getString("img_of_blog");
                Date dayPost = rs.getDate("daypost");
                String tag = rs.getString("tag");
                int numberOfLike = rs.getInt("number_of_like");
                int st = rs.getInt("status");
                CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(cobid);

                Blog blog = new Blog(id, managerId, courseId, cob, title, content, imgOfBlog, dayPost, tag, numberOfLike, st);
                list.add(blog);
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return list;
    }

    public Blog getBlog(int CategoryOfBlog_ID) {
        //logo 9
        Blog blog = new Blog();
        String sql = "SELECT * FROM Blog where CategoryOfBlog_ID = ?";
        try {

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, CategoryOfBlog_ID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                int managerId = rs.getInt("Manager_ID");
                int courseId = rs.getInt("Course_ID");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String imgOfBlog = rs.getString("img_of_blog");
                Date dayPost = rs.getDate("daypost");
                String tag = rs.getString("tag");
                int numberOfLike = rs.getInt("number_of_like");
                int status = rs.getInt("status");
                CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(CategoryOfBlog_ID);

                blog = new Blog(id, managerId, courseId, cob, title, content, imgOfBlog, dayPost, tag, numberOfLike, status);

            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return blog;
    }

    public Blog getBlogByID(int id) {
        //logo 9
        Blog blog = new Blog();
        String sql = "SELECT * FROM Blog where ID = ?";
        try {

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int managerId = rs.getInt("Manager_ID");
                int courseId = rs.getInt("Course_ID");
                int categoryOfBlogId = rs.getInt("CategoryOfBlog_ID");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String imgOfBlog = rs.getString("img_of_blog");
                Date dayPost = rs.getDate("daypost");
                String tag = rs.getString("tag");
                int numberOfLike = rs.getInt("number_of_like");
                int status = rs.getInt("status");
                CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(categoryOfBlogId);

                blog = new Blog(id, managerId, courseId, cob, title, content, imgOfBlog, dayPost, tag, numberOfLike, status);

            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return blog;
    }

    public void updateBlogByID(int id, String title, String tag, String content, String img) {
        String sql = "UPDATE Blog SET title = ?, tag = ?, content = ?, img_of_blog = ? where ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, tag);
            st.setString(3, content);
            st.setString(4, img);
            st.setInt(5, id);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public void editStatusByID(int id, int status) {
        String sql = "UPDATE Blog SET status = ? where ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public void AddBlog(int cobID, String title, String content, String img_of_blog, String tag) {
        String sql = "INSERT INTO Blog(CategoryOfBlog_ID, title, content, img_of_blog, daypost, tag, status)"
                + "VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cobID);
            st.setString(2, title);
            st.setString(3, content);
            st.setString(4, img_of_blog);
            st.setDate(5, new java.sql.Date(System.currentTimeMillis()));
            st.setString(6, tag);
            st.setInt(7, 1);
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public List<Blog> sophantutren1trang(List<Blog> list, int start, int end) {
        List<Blog> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {
        BlogDAO blogDAO = new BlogDAO();
//        blogDAO.updateBlogByID(16, "About us", "", "About Us", "About Us");
//        List<Blog> banners = blogDAO.getHome(7);
        List<Blog> banners = blogDAO.getBlogByStatus(-1);

        for (Blog banner : banners) {
            System.out.println(banner);
        }
//        Blog blog = blogDAO.getLogo();
//        System.out.println(blog);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.CategoryOfBlog;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nduya
 */
public class CategoryOfBlogDAO extends DBContext {

    public List<CategoryOfBlog> getAll() {
        String sql = "Select * from CategoryOfBlog";
        List<CategoryOfBlog> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CategoryOfBlog cob = new CategoryOfBlog(rs.getInt(1), rs.getString(2));
                list.add(cob);
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return list;
    }

    public CategoryOfBlog getCategoryOfBlogByID(int id) {
        String sql = "Select * from CategoryOfBlog where id = " + id;
        CategoryOfBlog cob = new CategoryOfBlog();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                cob = new CategoryOfBlog(rs.getInt(1), rs.getString(2));
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return cob;
    }

    public static void main(String[] args) {
        CategoryOfBlogDAO cobDAO = new CategoryOfBlogDAO();
        List<CategoryOfBlog> list = cobDAO.getAll();
        for (CategoryOfBlog categoryOfBlog : list) {
            System.out.println(categoryOfBlog);
        }
//        CategoryOfBlog cob = cobDAO.getCategoryOfBlogByID(10);
//        System.out.println(cob);
    }

}

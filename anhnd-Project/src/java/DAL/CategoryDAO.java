
package DAL;

import Model.CategoryOfCourse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CategoryDAO extends DBContext{
      public List<CategoryOfCourse> getAllCategory(){
    List<CategoryOfCourse> cat = new ArrayList<>();
    String sql = "SELECT * FROM CategoryOfCourse";
    try (Connection conn = connection;
         PreparedStatement st = conn.prepareStatement(sql);
         ResultSet rs = st.executeQuery()) {
        
        while (rs.next()) {
            CategoryOfCourse cate = new CategoryOfCourse();
            cate.setId(rs.getInt("id"));
            cate.setName(rs.getString("name"));
            cat.add(cate); // Thêm đối tượng CategoryOfCourse vào danh sách
        }
        
    } catch (SQLException ex) {
        System.out.println("Error: " + ex.getMessage());
    }
    return cat; // Trả về danh sách đã điền đầy đủ các đối tượng CategoryOfCourse
}

    public static void main(String[] args) {
    CategoryDAO sd = new CategoryDAO();
    List<CategoryOfCourse> li = sd.getAllCategory();
    if (!li.isEmpty()) {
        System.out.println(li.get(0).getName());
    } else {
        System.out.println("No categories found.");
    }
}
}

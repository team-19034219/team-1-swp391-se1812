/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Category;
import Model.Course;
import Model.Lecturers;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author nduya
 */
public class CourseDAO extends DBContext {

    private CategoryOfCourseDAO cd = new CategoryOfCourseDAO();
    private LecturersDAO ldao = new LecturersDAO();

    public List<Course> getAll() {
        List<Course> list = new ArrayList<>();
        String sql = "Select * From Course";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    //Search By Check
    public List<Course> searchByCheckBox(int[] cid) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1 ";
        if ((cid != null) && (cid[0] != 0)) {
            sql += " AND CategoryOfCourse_ID in(";
            for (int i = 0; i < cid.length; i++) {
                sql += cid[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ")";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByRadio(int id) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE CategoryOfCourse_ID=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    //searchbyname
    public List<Course> searchByName(String text) {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course where [name] like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Course> searchByCategory(int[] cid, double sPrice, double ePrice) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1 ";
        if (sPrice != 0) {
            sql += " and price >= " + sPrice;
        }
        if (ePrice != 0) {
            sql += " and price <= " + ePrice;
        }
        if ((cid != null) && (cid[0] != 0)) {
            sql += " AND CategoryOfCourse_ID in(";
            for (int i = 0; i < cid.length; i++) {
                sql += cid[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ")";
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByDiscount(double[] discount) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";

        // Check if the discount array is not null and not empty
        if (discount != null && discount.length > 0) {
            sql += " AND (";
            if (discount.length == 1) {
                sql += "discount <= ?";
            } else if (discount.length >= 2) {
                sql += "discount BETWEEN ? AND ?";
            }
            sql += ")";
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            if (discount != null && discount.length > 0) {
                if (discount.length == 1) {
                    st.setDouble(1, discount[0]);
                } else {
                    double lowerBound = discount[0];
                    double upperBound = discount[0];
                    for (int i = 1; i < discount.length; i++) {
                        if (discount[i] < lowerBound) {
                            lowerBound = discount[i];
                        }
                        if (discount[i] > upperBound) {
                            upperBound = discount[i];
                        }
                    }

                    st.setDouble(1, lowerBound);
                    st.setDouble(2, upperBound);
                }
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByDiscountAndCategory(double[] discount, int[] cid, double sPrice, double ePrice) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";

        // Check if the discount array is not null and not empty
        if (discount != null && discount.length > 0) {
            sql += " AND (";
            if (discount.length == 1) {
                sql += "discount <= ?";
            } else if (discount.length >= 2) {
                sql += "discount BETWEEN ? AND ?";
            }
            sql += ")";
        }
        if (sPrice != 0) {
            sql += " and price >= " + sPrice;
        }
        if (ePrice != 0) {
            sql += " and price <= " + ePrice;
        }
        if ((cid != null) && (cid[0] != 0)) {
            sql += " AND CategoryOfCourse_ID in(";
            for (int i = 0; i < cid.length; i++) {
                sql += cid[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ")";
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            if (discount != null && discount.length > 0) {
                if (discount.length == 1) {
                    st.setDouble(1, discount[0]);
                } else {
                    double lowerBound = discount[0];
                    double upperBound = discount[0];
                    for (int i = 1; i < discount.length; i++) {
                        if (discount[i] < lowerBound) {
                            lowerBound = discount[i];
                        }
                        if (discount[i] > upperBound) {
                            upperBound = discount[i];
                        }
                    }

                    st.setDouble(1, lowerBound);
                    st.setDouble(2, upperBound);
                }
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByDiscountAndCID(double[] discount, int[] cid) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";

        // Check if the discount array is not null and not empty
        if (discount != null && discount.length > 0) {
            sql += " AND (";
            if (discount.length == 1) {
                sql += "discount <= ?";
            } else if (discount.length >= 2) {
                sql += "discount BETWEEN ? AND ?";
            }
            sql += ")";
        }
        if ((cid != null) && (cid[0] != 0)) {
            sql += " AND CategoryOfCourse_ID in(";
            for (int i = 0; i < cid.length; i++) {
                sql += cid[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ")";
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            if (discount != null && discount.length > 0) {
                if (discount.length == 1) {
                    st.setDouble(1, discount[0]);
                } else {
                    double lowerBound = discount[0];
                    double upperBound = discount[0];
                    for (int i = 1; i < discount.length; i++) {
                        if (discount[i] < lowerBound) {
                            lowerBound = discount[i];
                        }
                        if (discount[i] > upperBound) {
                            upperBound = discount[i];
                        }
                    }

                    st.setDouble(1, lowerBound);
                    st.setDouble(2, upperBound);
                }
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByDiscountAndPrice(double[] discount, double sPrice, double ePrice) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";

        // Check if the discount array is not null and not empty
        if (discount != null && discount.length > 0) {
            sql += " AND (";
            if (discount.length == 1) {
                sql += "discount <= ?";
            } else if (discount.length >= 2) {
                sql += "discount BETWEEN ? AND ?";
            }
            sql += ")";
        }
        if (sPrice != 0) {
            sql += " and price >= " + sPrice;
        }
        if (ePrice != 0) {
            sql += " and price <= " + ePrice;
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            if (discount != null && discount.length > 0) {
                if (discount.length == 1) {
                    st.setDouble(1, discount[0]);
                } else {
                    double lowerBound = discount[0];
                    double upperBound = discount[0];
                    for (int i = 1; i < discount.length; i++) {
                        if (discount[i] < lowerBound) {
                            lowerBound = discount[i];
                        }
                        if (discount[i] > upperBound) {
                            upperBound = discount[i];
                        }
                    }

                    st.setDouble(1, lowerBound);
                    st.setDouble(2, upperBound);
                }
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchALL(int[] cid, double sPrice, double ePrice, String name, double[] discount, String order) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";
        if (sPrice != 0) {
            sql += " AND price >= ?";
        }
        if (ePrice != 0) {
            sql += " AND price <= ?";
        }
        if ((cid != null) && (cid[0] != 0)) {
            sql += " AND CategoryOfCourse_ID IN (";
            StringBuilder placeholders = new StringBuilder();
            for (int i = 0; i < cid.length; i++) {
                placeholders.append("?,");
            }
            sql += placeholders.deleteCharAt(placeholders.length() - 1).toString();
            sql += ")";
        }
        if (name != null) {
            sql += " AND name LIKE ?";
        }

        // Check if the discount array is not null and not empty
        if (discount != null && discount.length > 0) {
            sql += " AND (";
            if (discount.length == 1) {
                sql += "discount <= ?";
            } else if (discount.length >= 2) {
                sql += "discount BETWEEN ? AND ?";
            }
            sql += ")";
        }

        if (order != null && !order.isEmpty()) {
            // Split the order string by '+'
            String[] orderParts = order.split("\\+");
            // Join the parts with a space
            sql += " ORDER BY " + String.join(" ", orderParts);
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            int parameterIndex = 1;
            if (sPrice != 0) {
                st.setDouble(parameterIndex++, sPrice);
            }
            if (ePrice != 0) {
                st.setDouble(parameterIndex++, ePrice);
            }
            if ((cid != null) && (cid[0] != 0)) {
                for (int i = 0; i < cid.length; i++) {
                    st.setInt(parameterIndex++, cid[i]);
                }
            }
            if (name != null && !name.isEmpty()) {
                String sanitized = name.trim().replaceAll("\\s+", " ");
                st.setString(parameterIndex++, "%" + sanitized + "%");
            }
            if (discount != null && discount.length > 0) {
                if (discount.length == 1) {
                    st.setDouble(parameterIndex++, discount[0]);
                } else {
                    double lowerBound = discount[0];
                    double upperBound = discount[0];
                    for (int i = 1; i < discount.length; i++) {
                        if (discount[i] < lowerBound) {
                            lowerBound = discount[i];
                        }
                        if (discount[i] > upperBound) {
                            upperBound = discount[i];
                        }
                    }

                    st.setDouble(parameterIndex++, lowerBound);
                    st.setDouble(parameterIndex++, upperBound);
                }
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByNameAndPrice(double sPrice, double ePrice, String name) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1";
        if (sPrice != 0) {
            sql += " AND price >= ?";
        }
        if (ePrice != 0) {
            sql += " AND price <= ?";
        }
        if (name != null) {
            sql += " AND name LIKE ?";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            int parameterIndex = 1;
            if (sPrice != 0) {
                st.setDouble(parameterIndex++, sPrice);
            }
            if (ePrice != 0) {
                st.setDouble(parameterIndex++, ePrice);
            }
            if (name != null) {
                st.setString(parameterIndex++, "%" + name + "%");
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> searchByPrice(double sPrice, double ePrice) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM Course WHERE 1=1 ";
        if (sPrice != 0) {
            sql += " and price >= " + sPrice;
        }
        if (ePrice != 0) {
            sql += " and price <= " + ePrice;
        }
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> orderByPriceASC() {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course order by price asc";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> orderByPriceDesc() {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course order by price desc";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category ct = cd.getCategoryById(rs.getInt("CategoryOfCourse_ID"));
                Lecturers lecturers = ldao.getLecturerById(rs.getInt("Lecturer_ID"));
                Course c = new Course(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("img"), rs.getString("video_demo"), rs.getString("time_of_course"), rs.getInt("price"), rs.getDouble("discount"), rs.getDate("date"), ct, lecturers);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getMinPrice() {
        String sql = "SELECT price FROM Course WHERE price = (SELECT MIN(price) FROM Course);";
        int price = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                price = rs.getInt("price");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return price;
    }

    public int getMaxPrice() {
        String sql = "SELECT price FROM Course WHERE price = (SELECT MAX(price) FROM Course);";
        int price = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                price = rs.getInt("price");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return price;
    }

    public List<Course> sophantutren1trang(List<Course> list, int start, int end) {
        List<Course> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {
        CourseDAO cDAO = new CourseDAO();
        int[] cid = {1, 2};
//        List<Course> list = cDAO.searchByPrice(300000, 800000);
        double[] discount = {0.5, 0.2, 0.9};
        List<Course> list = cDAO.searchALL(cid, 30000, 2000000, "", discount, "order by name asc");
        for (Course course : list) {
            System.out.println(course);
        }
//        double price = cDAO.getMaxPrice();
//        System.out.println(price);
    }
}

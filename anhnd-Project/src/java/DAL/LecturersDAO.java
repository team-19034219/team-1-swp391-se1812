/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Lecturers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LecturersDAO extends DBContext {

    public List<Lecturers> getAllLectures() {
        List<Lecturers> lectures = new ArrayList<>();
        String sql = "SELECT * FROM Lecturers";

        try (Connection conn = connection; PreparedStatement st = conn.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                Lecturers lecture = new Lecturers();
                lecture.setId(rs.getInt("id"));
                lecture.setUsername(rs.getString("username"));
                lecture.setPassword(rs.getString("password"));
                lecture.setRole_ID(rs.getInt("role_ID"));
                lecture.setFullName(rs.getString("fullName"));
                lecture.setGender(rs.getInt("gender"));
                lecture.setAddress(rs.getString("address"));
                lecture.setEmail(rs.getString("email"));
                lecture.setPhone_number(rs.getString("phone_number"));
                lecture.setBirthday(rs.getDate("birthday"));
                lecture.setImg_certificates(rs.getString("img_certificates"));
                lecture.setImg_profile(rs.getString("img_profile"));
                lecture.setDescription(rs.getString("description"));

                lectures.add(lecture);
            }

        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        }

        return lectures;
    }

    public Lecturers getLecturerById(int id) {
        String sql = "select * from Lecturers where ID = ? AND status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Lecturers lecturers = new Lecturers(rs.getInt("ID"),rs.getString("username"), rs.getString("password"), rs.getInt("role_ID"), rs.getString("fullname"), rs.getInt("gender"), rs.getString("address"), rs.getString("email"), rs.getString("phone_number"), rs.getDate("birthday"), rs.getString("img_certificates"), rs.getString("img_profile"), rs.getString("description"),rs.getInt("status"));
                return lecturers;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void main(String[] args) {
        LecturersDAO ldao = new LecturersDAO();
//        List<Lecturers> list = ldao.getAllLectures();
        Lecturers lecturers = ldao.getLecturerById(1);
        System.out.println(lecturers);
    }
}

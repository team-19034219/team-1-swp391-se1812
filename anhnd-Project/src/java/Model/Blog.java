/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

public class Blog {

    private int id;
    private int managerId;
    private int courseId;
    private CategoryOfBlog categoryOfBlogId;
    private String title;
    private String content;
    private String imgOfBlog;
    private Date dayPost;
    private String tag;
    private int numberOfLike;
    private int status;

    // Default constructor
    public Blog() {
    }

    public Blog(int id, int managerId, int courseId, CategoryOfBlog categoryOfBlogId, String title, String content, String imgOfBlog, Date dayPost, String tag, int numberOfLike, int status) {
        this.id = id;
        this.managerId = managerId;
        this.courseId = courseId;
        this.categoryOfBlogId = categoryOfBlogId;
        this.title = title;
        this.content = content;
        this.imgOfBlog = imgOfBlog;
        this.dayPost = dayPost;
        this.tag = tag;
        this.numberOfLike = numberOfLike;
        this.status = status;
    } 
    
    // Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public CategoryOfBlog getCategoryOfBlogId() {
        return categoryOfBlogId;
    }

    public void setCategoryOfBlogId(CategoryOfBlog categoryOfBlogId) {
        this.categoryOfBlogId = categoryOfBlogId;
    }
    
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgOfBlog() {
        return imgOfBlog;
    }

    public void setImgOfBlog(String imgOfBlog) {
        this.imgOfBlog = imgOfBlog;
    }

    public Date getDayPost() {
        return dayPost;
    }

    public void setDayPost(Date dayPost) {
        this.dayPost = dayPost;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Blog{" + "id=" + id + ", managerId=" + managerId + ", courseId=" + courseId + ", categoryOfBlogId=" + categoryOfBlogId + ", title=" + title + ", content=" + content + ", imgOfBlog=" + imgOfBlog + ", dayPost=" + dayPost + ", tag=" + tag + ", numberOfLike=" + numberOfLike + ", status=" + status + '}';
    }
}

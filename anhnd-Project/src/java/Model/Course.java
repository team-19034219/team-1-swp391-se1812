/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author nduya
 */
public class Course {
    private int id, price;
    private String name, description, img, video, time_of_course;
    private double discount;
    private Date date;
    private Category category_id;
    private Lecturers lecturers_id;

    public Course() {
    }

    public Course(int id, String name, String description, String img, String video, String time_of_course, int price, double discount, Date date, Category category_id, Lecturers lecturers_id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.img = img;
        this.video = video;
        this.time_of_course = time_of_course;
        this.price = price;
        this.discount = discount;
        this.date = date;
        this.category_id = category_id;
        this.lecturers_id = lecturers_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTime_of_course() {
        return time_of_course;
    }

    public void setTime_of_course(String time_of_course) {
        this.time_of_course = time_of_course;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Category getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Category category_id) {
        this.category_id = category_id;
    }

    public Lecturers getLecturers_id() {
        return lecturers_id;
    }

    public void setLecturers_id(Lecturers lecturers_id) {
        this.lecturers_id = lecturers_id;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", name=" + name + ", description=" + description + ", img=" + img + ", video=" + video + ", time_of_course=" + time_of_course + ", price=" + price + ", discount=" + discount + ", date=" + date + ", category_id=" + category_id + ", lecturers_id=" + lecturers_id + '}';
    }
}

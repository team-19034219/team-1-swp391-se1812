<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="main.css" />
    </head>
    <body>
        <!--header area -->
        <jsp:include page="headeradmin.jsp"/>
        <!--header end -->

        <div class="container-fluid">
            <div class="row">
                <%@include file="navadmin.jsp" %>
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                        <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                    </div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                            <h1 class="h2">Setting Web</h1>
                        </div>
                        <div> 
                            <c:set var="currentCobId" value="${cobId}" />
                            <select id="categorySelect" class="form-select form-select-sm" style="width: 150px;" onchange="redirectToServlet()"> 
                                <c:forEach var="cob" items="${listcob}"> 
                                    <option value="${cob.id}" ${cob.id == currentCobId ? 'selected' : ''}>${cob.name}</option> 
                                </c:forEach> 
                            </select> 
                        </div> 
                        <div>
                            <select name="statusOrder" id="statusSelect" class="form-select form-select-sm" style="width: 100px;" onchange="redirectToServlet2()">
                                <option selected>Status</option>
                                <option value="1">Active</option>
                                <option value="0">Disable</option>
                            </select>
                        </div>
                        <div>
                            <form name="productsPerPageForm" action="managesetting" method="get">
                                <div class="product__widget-content d-flex">
                                    <div>
                                        <h6 class="product__widget-title">Products Per Page: </h6>
                                    </div>
                                    <div>
                                        <select name="productsPerPage" class="form-select form-select-sm" style="width: 60px ; margin-left: 10px;" onchange="this.form.submit()">
                                            <option value="8" ${param.productsPerPage == '8' ? 'selected' : ''}>8</option>
                                            <option value="9" ${param.productsPerPage == '9' ? 'selected' : ''}>9</option>
                                            <option value="10" ${param.productsPerPage == '10' ? 'selected' : ''}>10</option>
                                            <option value="11" ${param.productsPerPage == '11' ? 'selected' : ''}>11</option>
                                            <option value="12" ${param.productsPerPage == '12' ? 'selected' : ''}>12</option>
                                        </select>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div>
                            <input type="search" placeholder="Search any content">
                        </div>
                        <div class="d-flex justify-content-end">
                            <a id="submit" href="addsetting" class="btn btn-success mr-2"><i class="material-icons">&#xE147;</i> <span>Add New </span></a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Category of Blog</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Reset</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="listblog" items="${listblog}">
                                    <tr>
                                        <td>${listblog.id}</td>
                                        <td>${listblog.categoryOfBlogId.name}</td>
                                        <td>${listblog.title}</td>
                                        <td style="max-width: 250px">${listblog.content}</td>
                                        <td style="color: ${listblog.status == 0 ? 'red' : 'green'}">${listblog.status == 0 ? 'Inactive' : 'Active'}</td>
                                        <td>
                                            <a href ="editsetting?bID=${listblog.id}" >
                                                <button type="button" class="btn btn-warning">
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="16"
                                                        height="16"
                                                        fill="currentColor"
                                                        class="bi bi-pencil-square"
                                                        viewBox="0 0 16 16"
                                                        >
                                                    <path
                                                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"
                                                        ></path>
                                                    <path
                                                        fill-rule="evenodd"
                                                        d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"
                                                        ></path>
                                                    </svg>
                                                </button>
                                            </a>
                                        </td>
                                        <td >   
                                            <a href="managesetting?blogID=${listblog.id}&status=1" >
                                                <button type="button" class="btn btn-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bootstrap-reboot" viewBox="0 0 16 16">
                                                    <path d="M1.161 8a6.84 6.84 0 1 0 6.842-6.84.58.58 0 1 1 0-1.16 8 8 0 1 1-6.556 3.412l-.663-.577a.58.58 0 0 1 .227-.997l2.52-.69a.58.58 0 0 1 .728.633l-.332 2.592a.58.58 0 0 1-.956.364l-.643-.56A6.8 6.8 0 0 0 1.16 8z"/>
                                                    <path d="M6.641 11.671V8.843h1.57l1.498 2.828h1.314L9.377 8.665c.897-.3 1.427-1.106 1.427-2.1 0-1.37-.943-2.246-2.456-2.246H5.5v7.352zm0-3.75V5.277h1.57c.881 0 1.416.499 1.416 1.32 0 .84-.504 1.324-1.386 1.324z"/>
                                                    </svg>
                                                </button>
                                            </a>   
                                        </td>
                                        <td>
                                            <a href="managesetting?blogID=${listblog.id}&status=0" >
                                                <button type="button" class="btn btn-danger">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"></path>
                                                    <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"></path>
                                                    </svg>
                                                </button>
                                            </a>

                                        </td>
                                    </tr>
                                </c:forEach>                                
                            </tbody>
                        </table>
                    </div>
                    <nav aria-label="Page navigation example">
                        <c:set var="page" value="${page}"/>
                        <c:set var="totalPages" value="${num}"/>
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="managesetting?page=${page - 1}&productsPerPage=${numberpage}&statusOrder=${statusOrder}">
                                    <i class="fal fa-chevron-double-right"></i> Previous
                                </a>
                            </li>
                            <c:forEach begin="1" end="${totalPages}" var="i">
                                <li class="page-item ${i == page ? 'active' : ''}">
                                    <a class="page-link" href="managesetting?page=${i}&productsPerPage=${numberpage}&statusOrder=${statusOrder}">
                                        ${i}
                                    </a>
                                </li>
                            </c:forEach>
                            <li class="page-item">
                                <a class="page-link" href="managesetting?page=${page + 1}&productsPerPage=${numberpage}&statusOrder=${statusOrder}">
                                    <i class="fal fa-chevron-double-right"></i> Next
                                </a>
                            </li>
                        </ul>                    
                    </nav>                            
                </main>
            </div>
        </div>
        <script>
            function redirectToServlet() {
                var select = document.getElementById('categorySelect');
                var selectedValue = select.value;
                if (selectedValue) {
                    window.location.href = 'managesetting?cobID=' + selectedValue;
                }
            }
            
            function redirectToServlet2() {
                var select = document.getElementById('statusSelect');
                var selectedValue = select.value;
                if (selectedValue) {
                    window.location.href = 'managesetting?statusOrder=' + selectedValue;
                }
            }
        </script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
            crossorigin="anonymous"
        ></script>
    </body>
</html>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Home</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="main.css" />
        <style>
            .category{
                justify-content: space-between;
            }
        </style>


        <!--header area -->
        <jsp:include page="header.jsp"/>
        <!--header end -->

        <!-- banner -->
    <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner" >
            <c:forEach var="banner" items="${banner}">

                <div class="carousel-item active" >
                    <img src="${banner.imgOfBlog}" style="height: 600px" class="d-block w-100" alt="..." />
                </div>

            </c:forEach>
        </div>

        <button
            class="carousel-control-prev btn btn-light"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="prev"
            >
            <span class="carousel-control-prev-icon " aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button
            class="carousel-control-next btn btn-light"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="next"
            >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>


    <!-- Slogan -->

    <div class="container px-4 py-5" id="hanging-icons">
        <!-- <h2 class="pb-2 border-bottom">Hanging icons</h2> -->
        <div class="row g-4 py-5 row-cols-1 row-cols-lg-3">

            <c:forEach var="s" items="${slogan}">

                <div class="col d-flex align-items-start">

                    <div
                        class="icon-square text-body-emphasis bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3"
                        >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            height="30"
                            fill="currentColor"
                            class="bi bi-check2-circle"
                            viewBox="0 0 16 16"
                            >
                        <path
                            d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"
                            ></path>
                        <path
                            d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"
                            ></path>
                        </svg>
                    </div>
                    <div>

                        <h3 class="fs-2 text-body-emphasis">${s.title}</h3>
                        <p>${s.content}</p>
                        <!-- <a href="#" class="btn btn-primary"> Primary button </a> -->

                    </div>

                </div>

            </c:forEach>

        </div>

    </div>

    <!-- Category -->
    <div class="album  bg-light">
        <h2 class="text-body-emphasis text-center py-3">
            What do you want to learn today ?
        </h2>
        <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <c:forEach var="cate" items="${cate}">
                    <div class="col-md-3">
                        <div class="card shadow-sm">
                            <img src="images/c++.jpg" alt="">
                            <div class="card-body">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="btn-group">
                                        <a href="#">
                                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                                ${cate.name}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>




    <!-- Top courses 2024 -->
    <div class="album py-3 bg-light">
        <h2 class="text-body-emphasis text-center py-3">Top courses in 2024</h2>
        <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <!-- Iterate over courses -->
                <c:forEach var="courses" items="${courses}">
                    <div class="col-md-3">
                        <div class="card shadow-sm">
                            <img src="images/c++.jpg" alt="">
                            <div class="card-body">
                                <p class="card-text"><strong>${courses.name}</strong></p>
                                <p></p>
                                <p class="d-inline text-decoration-line-through">${courses.price}</p>
                                <p class="d-inline">${courses.getPriceDiscount()} vnd</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                        <button type="button" class="btn btn-sm btn-outline-secondary">Add to cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach> <!-- End of forEach -->
            </div>
        </div>
    </div>



    <!-- news -->
    <div class="album py-3 bg-light">
        <h2 class="text-body-emphasis text-center py-3">
            Top Instructor
        </h2>
        <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

                <c:forEach var="lectures" items="${lectures}">
                    <div class="col-md-2">
                        <div class="card shadow-sm">
                            <img src="images/instructor.jpg" alt="">
                            <div class="card-body">
                                <div class="btn-group d-flex flex-column-reverse justify-content-center align-items-center">
                                    <a href="viewinstuctor?id=${lectures.id}">
                                        <button type="button" class="btn btn-sm btn-outline-secondary">
                                            ${lectures.fullName}
                                        </button>
                                    </a>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>




    <!-- footer -->

    <!--footer area start-->
    <jsp:include page="footer.jsp"/>
    <!--footer area end-->

    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"
    ></script>
</body>
</html>

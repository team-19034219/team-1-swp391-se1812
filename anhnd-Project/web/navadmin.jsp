<%-- 
    Document   : navadmin
    Created on : May 26, 2024, 11:55:38 AM
    Author     : bachq
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <style>
            .nav-item:hover{
                text-decoration: underline;
                /*background-color: orange;*/
            }
        </style>
    </head>
    <body>
        <nav
            id="sidebarMenu"
            class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
            >
            <div class="position-sticky pt-3">
                <ul class="nav flex-column" style="margin-left: 5px">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            Dashboard
                        </a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Manager 
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">

                            Student
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Lecturers
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">


                            Teacher Assistant
                        </a>
                    </li>




                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Course
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Orders
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Sale
                        </a>
                    </li>

                    <!--                    <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="designWebDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                Final Test
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="designWebDropdown">
                                                <li><a class="dropdown-item" href="#">Test</a></li>
                                                <li><a class="dropdown-item" href="#">Question Bank</a></li>
                                            </ul>
                                        </li>-->

                </ul>

                <div class="accordion accordion-flush" id="accordionFlushExample1">
                    <div class="accordion-item" style="margin-right: 10px">
                        <h2 class="accordion-header" id="flush-headingOne1">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne1" aria-expanded="false" aria-controls="flush-collapseOne1" style="background-color: #F8F9FA; color: #0D6EFD">
                                Manage Blog
                            </button>
                        </h2>
                        <div id="flush-collapseOne1" class="accordion-collapse collapse" aria-labelledby="flush-headingOne1" data-bs-parent="#accordionFlushExample1" style="background-color: #F8F9FA; color: #0D6EFD">
                            <div class="accordion-body"><a href="#">List of Blog</a></div>
                            <div class="accordion-body"><a href="#">Add new</a></div>
                        </div>
                    </div>
                </div>

                <div class="accordion accordion-flush" id="accordionFlushExample2">
                    <div class="accordion-item" style="margin-right: 10px">
                        <h2 class="accordion-header" id="flush-headingOne2">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne2" aria-expanded="false" aria-controls="flush-collapseOne2" style="background-color: #F8F9FA; color: #0D6EFD ">
                                Final Test
                            </button>
                        </h2>
                        <div id="flush-collapseOne2" class="accordion-collapse collapse" aria-labelledby="flush-headingOne2" data-bs-parent="#accordionFlushExample2" style="background-color: #F8F9FA; color: #0D6EFD">
                            <div class="accordion-body"><a href="#">Manage Test</a></div>
                            <div class="accordion-body"><a href="#">Manager Question Bank</a></div>
                        </div>
                    </div>
                </div>



            </div>
        </nav>

    </body>
</html>

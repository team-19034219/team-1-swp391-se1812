<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="main.css" />
    </head>
    <body>
        <!--header area -->
        <jsp:include page="headeradmin.jsp"/>
        <!--header end -->

        <div class="container-fluid">
            <div class="row">
                <%@include file="navadmin.jsp" %>
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                        <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                    </div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                            <h1 class="h2">Update Blog</h1>
                        </div>
                        <!--            <div class="col-sm-6 d-flex justify-content-end">
                                      <a id="submit" href="" class="btn btn-success mr-2"><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>
                                      <a id="submit" href="" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>
                                    </div>-->
                    </div>
                    <div class="container">
                        <div class="main-body">
                                <!-- /Breadcrumb -->
                                <form action="editsetting" method="post">
                                    <div class="row gutters-sm">
                                        <div class="col-md-4 mb-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="d-flex flex-column align-items-center text-center">
                                                        <img src="${blog.imgOfBlog}" alt="Admin" width="150">
                                                    <div class="mt-3">
                                                        <input type="file" name="img" id="form_file" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-8">
                                        <div class="card mb-3">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-9 text-secondary">
                                                        <input value="${blog.id}" name="blogID" type="hidden" class="form-control acceptEdit">
                                                    </div>
                                                </div>

                                                <!--                                            <div class="row">
                                                                                                <div class="col-sm-3">
                                                                                                    <h6 class="mb-0">Category of Blog</h6>
                                                                                                </div>
                                                                                                <div class="col-sm-9 text-secondary">
                                                                                                    <select class="form-select form-select-sm" style="width: 150px;">
                                                                                                        <option selected>Category of Blog</option>
                                                                                                        <option>Header</option>
                                                                                                        <option>Banner</option>
                                                                                                        <option>Slogan</option>
                                                                                                        <option>Footer</option>
                                                                                                        <option>Oder By</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                
                                                                                            <hr>-->

                                                <!--                                            <div class="row">
                                                                                                <div class="col-sm-3">
                                                                                                    <h6 class="mb-0">Content</h6>
                                                                                                </div>
                                                                                                <div class="col-sm-9 text-secondary">
                                                                                                    <input class="form-control acceptEdit" 
                                                                                                           name="name" id="inputFirstName" type="text">
                                                                                                </div>
                                                                                            </div>
                                                
                                                                                            <hr>-->

                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Title</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input class="form-control acceptEdit"  name="title" value="${blog.title}" id="inputFirstName" type="text">

                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Tag</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <input class="form-control acceptEdit" name="tag" value="${blog.tag}" id="inputFirstName" type="text">

                                                    </div>
                                                </div>
                                                <hr>


                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <h6 class="mb-0">Description</h6>
                                                    </div>
                                                    <div class="col-sm-9 text-secondary">
                                                        <textarea class="form-control acceptEdit" rows="6" name="content" id="inputFirstName" style="font-family: inherit;">${blog.content}</textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                                <!--                                            <div class="row">
                                                                                                <div class="col-sm-3">
                                                                                                    <h6 class="mb-0">Sale</h6>
                                                                                                </div>
                                                                                                <div class="col-sm-9 text-secondary">
                                                                                                    <input class="form-control acceptEdit" name="name" id="inputFirstName" type="text">
                                                                                                </div>
                                                                                            </div>
                                        <hr>-->
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <!-- <a class="btn btn-info " target="__blank" href="">Edit</a> -->
                                                        <button type="submit" class="btn btn-primary" >Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
            crossorigin="anonymous"
        ></script>
    </body>
</html>

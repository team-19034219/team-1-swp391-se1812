<%-- 
    Document   : viewcategory
    Created on : May 15, 2024, 8:22:10 AM
    Author     : nduya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="main.css" />
        <style>
            .CategoryCh{
                margin-top: 20px;
                margin-left: 40px;
            }
            .btn {
                margin-bottom: 5px;
            }

            .grid {
                position: relative;
                width: 100%;
                background: #fff;
                color: #666666;
                border-radius: 2px;
                margin-bottom: 25px;
                box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
            }

            .grid .grid-body {
                padding: 15px 20px 15px 20px;
                font-size: 0.9em;
                line-height: 1.9em;
            }

            .search table tr td.rate {
                color: #f39c12;
                line-height: 50px;
            }

            .search table tr:hover {
                cursor: pointer;
            }

            .search table tr td.image {
                width: 50px;
            }

            .search table tr td img {
                width: 50px;
                height: 50px;
            }

            .search table tr td.rate {
                color: #f39c12;
                line-height: 50px;
            }

            .search table tr td.price {
                font-size: 1.5em;
                line-height: 50px;
            }

            .search #price1,
            .search #price2 {
                display: inline;
                font-weight: 600;
            }

            input[data-type="currency"] {
                width: 40%;            /* Full width */
                padding: 2px;          /* Padding inside the input */
                margin: 2px 0;         /* Margin outside the input */
                box-sizing: border-box; /* Include padding and border in the element's total width and height */
                border: 2px solid #ccc; /* Border style */
                border-radius: 4px;     /* Rounded corners */
                font-size: 15px;        /* Font size */
                font-family: Arial, sans-serif; /* Font family */
                color: #333;            /* Text color */
                background-color: #f9f9f9; /* Background color */
                transition: border-color 0.3s ease; /* Transition for border color change */
            }

            /* Focus state */
            input[data-type="currency"]:focus {
                border-color: #66afe9;  /* Border color when focused */
                outline: none;          /* Remove default outline */
                background-color: #fff; /* Background color when focused */
            }

            /* Disabled state */
            input[data-type="currency"]:disabled {
                background-color: #e9e9e9; /* Background color when disabled */
                cursor: not-allowed;       /* Cursor style when disabled */
            }

            /* Placeholder text */
            input[data-type="currency"]::placeholder {
                color: #999;              /* Placeholder text color */
                font-style: italic;       /* Placeholder text style */
            }

            /* Invalid state */
            input[data-type="currency"]:invalid {
                border-color: #e74c3c;    /* Border color when input is invalid */
                background-color: #f9dcdc; /* Background color when input is invalid */
            }

        </style>
    </head>
    <body>
        <!--header area -->
        <jsp:include page="header.jsp"/>
        <!--header end -->

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <div class="container">
            <div class="row">
                <!-- BEGIN SEARCH RESULT -->
                <div class="col-md-12">
                    <div class="grid search">
                        <div class="grid-body">
                            <div class="row">
                                <!-- BEGIN FILTERS -->
                                <div class="col-md-3">
                                    <h2 class="grid-title"><i class="fa fa-filter"></i> Filters</h2>
                                    <hr>

                                    <form id="myForm" action="viewcategory" method="get">
                                        <!-- BEGIN FILTER BY CATEGORY -->
                                        <h4>By category:</h4>
                                        <div class="responsiveFacets_sectionItemLabel">
                                            <input type="checkbox" class="responsiveFacets_sectionItemCheckbox" 
                                                   ${chid[0]?"checked":""} 
                                                   id="c0" 
                                                   name="cid_refinee"
                                                   value="${0}" onclick="setCheck(this)">
                                            <label class="responsiveFacets_sectionItem" for="brand">
                                                ALL (${listcategory.size()})
                                            </label>
                                        </div>
                                        <c:if test="${listcategory!=null}">
                                            <c:forEach begin="0" end="${listcategory.size()-1}" var="i">
                                                <div class="responsiveFacets_sectionItemLabel">
                                                    <input
                                                        type="checkbox"
                                                        ${listcategory.get(i).getId()==cid_refine?"checked":""}
                                                        class="responsiveFacets_sectionItemCheckbox"
                                                        id="cm" 
                                                        name="cid_refinee"
                                                        value="${listcategory.get(i).getId()}"
                                                        ${chid[i+1]?"checked":""}
                                                        onclick="setCheck(this)"/>
                                                    <label class="responsiveFacets_sectionItem" for="brand" >
                                                        ${listcategory.get(i).name}
                                                    </label>
                                                </div>
                                            </c:forEach>
                                        </c:if>

                                        <!-- END FILTER BY CATEGORY -->

                                        <div class="padding"></div>



                                        <div class="padding"></div>
                                        <h4>By rate:</h4>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                        </div>




                                        <!-- BEGIN FILTER BY PRICE -->
                                        <h4>By price:</h4>
                                        Between <div id="price1">${sprice}</div> to <div id="price2">${eprice}</div><br>
                                        <input type="text" id="priceFrom" name="pricefrom" value="${sprice}" min="${minprice}" max="${maxprice}" data-type="currency" data-min="${minprice}" data-max="${maxprice}">
                                        <input type="text" id="priceTo" name="priceto" value="${eprice}" min="${minprice}" max="${maxprice}" data-type="currency" data-min="${minprice}" data-max="${maxprice}">
                                        <!-- END FILTER BY PRICE -->

                                        <!-- BEGIN FILTER BY DISCOUNT -->
                                        <h4>By Discount:</h4>
                                        <div class="responsiveFacets_sectionItemLabel">
                                            <input type="checkbox" class="responsiveFacets_sectionItemCheckbox" 
                                                   ${chid2[0]?"checked":""} 
                                                   id="c1" 
                                                   name="discount"
                                                   value="1" onclick="setCheck(this)">
                                            <label class="responsiveFacets_sectionItem" for="brand">
                                                ALL
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="icheck" name="discount" value="0.5" ${chid2[1] ? "checked" : ""} onclick="setCheck(this)"> 50%
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="icheck" name="discount" value="0.3" ${chid2[2] ? "checked" : ""} onclick="setCheck(this)"> 30%
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="icheck" name="discount" value="0.2" ${chid2[3] ? "checked" : ""} onclick="setCheck(this)"> 20%
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="icheck" name="discount" value="0.1" ${chid2[4] ? "checked" : ""} onclick="setCheck(this)"> 10%
                                            </label>
                                        </div>
                                        <!-- END FILTER BY DISCOUNT -->


                                        <!-- Button -->
                                        <button class="submit-price" style="display: none">Apply</button>
                                    </form>
                                    <hr>
                                    <form name="productsPerPageForm" action="viewcategory" method="get">
                                        <div class="product__widget-content d-flex">
                                            <div>
                                                <h4 class="product__widget-title">Products Per Page: </h4>
                                            </div>
                                            <div>
                                                <select name="productsPerPage" class="form-select form-select-sm" style="width: 60px ; margin-left: 10px;" onchange="this.form.submit()">
                                                    <option value="8" ${param.productsPerPage == '8' ? 'selected' : ''}>8</option>
                                                    <option value="9" ${param.productsPerPage == '9' ? 'selected' : ''}>9</option>
                                                    <option value="10" ${param.productsPerPage == '10' ? 'selected' : ''}>10</option>
                                                    <option value="11" ${param.productsPerPage == '11' ? 'selected' : ''}>11</option>
                                                    <option value="12" ${param.productsPerPage == '12' ? 'selected' : ''}>12</option>
                                                </select>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END FILTERS -->
                                <!-- BEGIN RESULT -->
                                <div class="col-md-9">
                                    <c:if test="${txtSearch != null}">
                                        <p>Showing all results matching "${txtSearch}"</p>
                                    </c:if>

                                    <div class="padding"></div>

                                    <div class="row">
                                        <!-- BEGIN ORDER RESULT -->
                                        <div class="col-sm-6">
                                            <div class="btn-group">
                                                <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown">
                                                    ${getorder.name}<i class="bi bi-caret-down-fill"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <c:forEach var="lorder" items="${requestScope.listOrder}">
                                                        <li>
                                                            <a class="dropdown-item" href="#" onclick="setOrder('${lorder.tag}')">${lorder.title}</a>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- END ORDER RESULT -->

                                        <form id="orderForm" action="viewcategory" method="get" style="display: none;">
                                            <input type="hidden" name="orderBy" id="orderBy">
                                        </form> 
                                    </div>

                                    <!-- BEGIN TABLE RESULT -->
                                    <div class="table-responsive" id ="content">
                                        <c:forEach var="lcourse" items="${listcourse}">
                                            <div class="format">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td class="image"><img src="images/c.jpg" alt=""></td>
                                                            <td class="viewcategory" style="width: 500px"><strong>${lcourse.name}</strong><br>${lcourse.description}</td>
                                                            <td class="rate text-right"><span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span></td>
                                                            <td class="price text-right" > 
                                                                <input type="hidden" class="amount" value="${lcourse.price}">
                                                                <span class="num currency price"></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- END TABLE RESULT -->

                                    <!-- BEGIN PAGINATION -->
                                    <nav aria-label="Page navigation example">
                                        <c:set var="page" value="${page}"/>
                                        <c:set var="totalPages" value="${num}"/>
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="viewcategory?page=${page - 1}&productsPerPage=${numberpage}&pricefrom=${sprice}&priceto=${eprice}&order=${order == null ? '' : order}">
                                                    <i class="fal fa-chevron-double-right"></i> Previous
                                                </a>
                                            </li>
                                            <c:forEach begin="1" end="${totalPages}" var="i">
                                                <li class="page-item ${i == page ? 'active' : ''}">
                                                    <a class="page-link" href="viewcategory?page=${i}&productsPerPage=${numberpage}&pricefrom=${sprice}&priceto=${eprice}&order=${order == null ? '' : order}">
                                                        ${i}
                                                    </a>
                                                </li>
                                            </c:forEach>
                                            <li class="page-item">
                                                <a class="page-link" href="viewcategory?page=${page + 1}&productsPerPage=${numberpage}&pricefrom=${sprice}&priceto=${eprice}&order=${order == null ? '' : order}">
                                                    <i class="fal fa-chevron-double-right"></i> Next
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                    <!-- END PAGINATION -->
                                </div>
                                <!-- END RESULT -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SEARCH RESULT -->
            </div>
        </div>
        <!--footer area start-->
        <jsp:include page="footer.jsp"/>
        <!--footer area end-->

        <!-- all js here -->
        <script>
            function updatePriceRange(value) {
                document.getElementById('price2').innerText = value;
                document.getElementById('priceTo').value = value;
            }

            function setCheck(obj) {
                var fries = document.getElementsByName('cid_refinee');
                if ((obj.id == 'c0') && (fries[0].checked) == true) {
                    for (var i = 1; i < fries.length; i++) {
                        fries[i].checked = false;
                    }
                } else {
                    for (var i = 1; i < fries.length; i++) {
                        if (fries[i].checked == true) {
                            fries[0].checked = false;
                            break;
                        }
                    }
                }

                var checkboxes = document.getElementsByName('discount');

                if (obj.id === 'c1' && (checkboxes[0].checked) == true) {
                    // Uncheck all other checkboxes except the current one
                    for (var i = 1; i < checkboxes.length; i++) {
                        checkboxes[i].checked = false;
                    }
                } else {
                    for (var i = 1; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked == true) {
                            checkboxes[0].checked = false;
                            break;
                        }
                    }
                }

                document.getElementById('myForm').submit();
            }

            document.addEventListener("DOMContentLoaded", function () {
                // Function to format and update price display
                function formatAndSetPrice(elementId, value) {
                    let formatter = new Intl.NumberFormat("vi-VN", {
                        style: "currency",
                        currency: "VND"
                    });
                    document.getElementById(elementId).innerText = formatter.format(value);
                }

                // Initialize price range display
                let sprice = parseFloat(document.getElementById('priceFrom').value);
                let eprice = parseFloat(document.getElementById('priceTo').value);
                formatAndSetPrice('price1', sprice);
                formatAndSetPrice('price2', eprice);

                // Select all elements with the class 'format'
                let priceElements = document.querySelectorAll('.format');
                // Loop through each price element
                priceElements.forEach(function (priceElement) {
                    // Get the amount from the hidden input inside this price element
                    let amount = parseFloat(priceElement.querySelector(".amount").value);
                    // Create a new formatter for each price element
                    let formatter = new Intl.NumberFormat("vi-VN", {
                        style: "currency",
                        currency: "VND"
                    });
                    // Format the amount and set it as the inner text of the price element
                    priceElement.querySelector(".num").innerText = formatter.format(amount);
                });

                // Update displayed prices when input values change
                document.getElementById('priceFrom').addEventListener('input', function () {
                    let newSprice = parseFloat(this.value);
                    formatAndSetPrice('price1', newSprice);
                });

                document.getElementById('priceTo').addEventListener('input', function () {
                    let newEprice = parseFloat(this.value);
                    formatAndSetPrice('price2', newEprice);
                });
            });

            function setDiscount(obj) {
                var checkboxes = document.getElementsByName('discount');

                if (obj.id === 'c0' && obj.checked) {
                    // Uncheck all other checkboxes except the current one
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i] !== obj) {
                            checkboxes[i].checked = false;
                        }
                    }
                }
                document.getElementById('myForm').submit();
            }
            function setOrder(orderBy) {
                document.getElementById('orderBy').value = orderBy;
                document.getElementById('orderForm').submit();
            }
            window.onload = function () {
                if (performance.getEntriesByType("navigation")[0].type === "reload") {
                    // Page was reloaded, send a request to the servlet to clear the session
                    window.location.href = "reload";
                }
            };
        </script>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var currencyInputs = document.querySelectorAll('input[data-type="currency"]');
                var currency = 'VND'; // Setting currency to VND

                // Format initial values
                currencyInputs.forEach(function (input) {
                    onBlur({target: input});
                });
                // Bind event listeners
                currencyInputs.forEach(function (input) {
                    input.addEventListener('focus', onFocus);
                    input.addEventListener('blur', onBlur);
                    input.addEventListener('input', onInput);
                });
                
                function localStringToNumber(s) {
                    return Number(String(s).replace(/[^0-9.]+/g, "")); // Modified to allow numbers and decimal points
                }

                function onFocus(e) {
                    var value = e.target.value;
                    e.target.value = '';
                }

                function onBlur(e) {
                    var value = e.target.value;
                    if (isPositiveInteger(value)) {
                        var options = {
                            currency: currency,
                            style: "currency",
                            currencyDisplay: "symbol",
                            useGrouping: true
                        };
                        e.target.value = (value || value === 0)
                                ? localStringToNumber(value).toLocaleString('vi-VN', options) // Using 'vi-VN' locale for VND
                                : '';
                    } else {
                        e.target.value = '';
                    }
                }

                function onInput(e) {
                    var value = e.target.value;
                    e.target.value = value.replace(/[^\d]/g, ''); // Remove any non-digit characters
                }

                function isPositiveInteger(value) {
                    var number = localStringToNumber(value);
                    return Number.isInteger(number) && number > 0;
                }
            });
        </script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
            crossorigin="anonymous"
        ></script>

    </body>
</html>
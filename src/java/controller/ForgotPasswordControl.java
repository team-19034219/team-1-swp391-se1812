/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.StudentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import model.Students;

/**
 *
 * @author nguye
 */
public class ForgotPasswordControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ForgotPasswordControl</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ForgotPasswordControl at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public static boolean isValidEmail(String email) {
        String emailPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    protected void sendEmail(HttpServletRequest request, HttpServletResponse response, String email)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String email1 = "";

        final String username = "nghiahtsdad@gmail.com";
        final String password = "lgcl lzbj lsxv gjuj";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587"); //port 25 465
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        props.put("mail.smtp.starttls.enable", "true");

        Session session2 = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session2);
            message.addHeader("Context-type", "text/html;charset=UTF-8");
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(MimeUtility.encodeText("You want to change your password!", "UTF-8", "B"));

            if (session.getAttribute("checkemail") != null) {
                email1 = (String) session.getAttribute("checkemail");
            }

            String htmlContent = "<div style=\" margin-left: 36%; padding: 10px; width: 400px; border: solid 1px gainsboro; border-radius: 10px;\">"
                    + "<div style=\"margin-left: 150px;\"><img src=\"https://i.postimg.cc/50fJXxTQ/logo2.jpg\" style=\"width: 100px;\"></div>\n"
                    + "<div><p style=\"font-weight: bold; font-size: 20px; text-align: center;\">Change password request</p></div>\n"
                    + "<div><p style=\"text-align: center;\">" + email1 + "</p></div>\n"
                    + "<div><p>We noticed a new request to change password to your Google Account on a Windows device."
                    + "If this was you, you can click button to change your password. If not, we'll help you secure your account.</p></div>\n"
                    + "<div style=\"margin-left: 30%\"><a href=\"http://localhost:8080/Demo1/change\" style=\"background-color: #4185F4; color: white; padding: 10px; border-radius: 10px; text-decoration: none; display: inline-block;\">Change your password</a></div>\n"
                    + "</div>";

            message.setContent(htmlContent, "text/html;charset=UTF-8");

            Transport.send(message);

//            System.out.println("Email sent successfully.");
        } catch (MessagingException e) {
            throw new ServletException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("forgotpass.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        StudentDAO info = new StudentDAO();
        List<Students> list = info.getAllStudent();
        int check = 0;
        String validMessage;
        try {
            String email = request.getParameter("email");
            if (!isValidEmail(email)) {
                throw new ArithmeticException();
            }
            for (Students student : list) {
                if (student.getEmail().equals(email)) {
                    check = 1;
                    session.setAttribute("checkemail", email);
                    session.setMaxInactiveInterval(3 * 60);
                    sendEmail(request, response, email);
                    request.setAttribute("sucess", "Check your mail to countinous change password and must change in a 3 minute!");
                    request.getRequestDispatcher("forgotpass.jsp").forward(request, response);
                }
            }
            if (check == 0) {
                throw new NumberFormatException();
            }

        } catch (NumberFormatException e) {
            validMessage = "Email is not correct. Please enter again!";
            request.setAttribute("error", validMessage);
            request.getRequestDispatcher("forgotpass.jsp").forward(request, response);
        } catch (ArithmeticException e) {
            validMessage = "Email you enter is not valid. Please enter again!";
            request.setAttribute("error", validMessage);
            request.getRequestDispatcher("forgotpass.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

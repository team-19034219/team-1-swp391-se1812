/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.User;
import dal.StudentDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Students;

/**
 *
 * @author nguye
 */
public class LoginWithFacebook extends HttpServlet {

    private static final String APP_ID = "263486040190907";
    private static final String APP_SECRET = "9db5f2726d9df4012a205b7bdf1babef";
    private static final String REDIRECT_URI = "http://localhost:8080/Demo1/loginwithfb";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String code = request.getParameter("code");
        int tmp = 0;
        HttpSession session = request.getSession();
        if (code == null || code.isEmpty()) {
            response.sendRedirect("login");
            return;
        }

        String accessToken = getFacebookAccessToken(code);
        StudentDAO student = new StudentDAO();
        List<Students> list = student.getAllStudent();
        if (accessToken != null) {
            User user = getFacebookUserProfile(accessToken);
            for (Students students : list) {
                if (students.getEmail().equals(user.getEmail())) {
                    session.setAttribute("role", students.getRoll_ID());
                    session.setAttribute("sid", students.getId());
                    session.setMaxInactiveInterval(60 * 60 * 60 * 60);
                    tmp = 1;
                    response.sendRedirect("home");

                }
            }
            if (tmp == 0) {
                session.setAttribute("role", list.get(list.size() - 1).getRoll_ID());
                session.setAttribute("sid", list.get(list.size() - 1).getId());
                session.setMaxInactiveInterval(60 * 60 * 60 * 60);
                response.sendRedirect("home");
            }
        } else {
            response.sendRedirect("login");
        }
    }

    private String getFacebookAccessToken(String code) {
        try {
            String accessTokenUrl = "https://graph.facebook.com/v12.0/oauth/access_token?client_id=" + APP_ID
                    + "&redirect_uri=" + REDIRECT_URI
                    + "&client_secret=" + APP_SECRET
                    + "&code=" + code;

            // Here you would make an HTTP GET request to accessTokenUrl and parse the response
            // to get the access token. This is a placeholder for actual implementation.
            return "PLACEHOLDER_ACCESS_TOKEN"; // Replace with actual access token.
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private User getFacebookUserProfile(String accessToken) {
        FacebookClient facebookClient = new DefaultFacebookClient(accessToken, Version.LATEST);
        return facebookClient.fetchObject("me", User.class, Parameter.with("fields", "name,email"));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.StudentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import java.util.regex.Pattern;
import model.Students;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author nguye
 */
public class ChangePassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private boolean isValidPassword(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        }
        String emailRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(password).matches();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("changepass.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String validMessage;
        String getemail = null;
        String password;
        String confirm;
        try {
            getemail = request.getParameter("getemail");
            password = request.getParameter("password");
            confirm = request.getParameter("confirm");

            StudentDAO student = new StudentDAO();
            List<Students> listStudent = student.getAllStudent();

            if (!password.equals(confirm)) {
                validMessage = "The password and confirm must be same, Please not enter space !";
                request.setAttribute("error", validMessage);
                throw new NumberFormatException();
            }

            if (!isValidPassword(password)) {
                validMessage = "Password must contain at least 1 uppercase letter, 1 lowercase letter, 1 numeric character, and a minimum length of 6!";
                request.setAttribute("error", validMessage);
                throw new NumberFormatException();
            }

            int sid = 0;

            for (Students students : listStudent) {
                if (students.getEmail().equals(getemail)) {
                    if (BCrypt.checkpw(password, students.getPassword())) {
                        validMessage = "The new password and old password must be different,Please not enter space !";
                        request.setAttribute("error", validMessage);
                        throw new NumberFormatException();
                    } else {
                        sid = students.getId();
                    }
                }
            }

            if (sid != 0) {
                String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
                student.editPassword(sid, hashed);
                session.removeAttribute("checkemail");
//                request.setAttribute("Message", "Change Password Sucessfully!");
//                request.getRequestDispatcher("login.jsp").forward(request, response);
                response.sendRedirect("login");
            }

        } catch (NumberFormatException e) {
            session.setAttribute("checkemail", getemail);
            session.setMaxInactiveInterval(60 * 60 * 60 * 60);
            request.getRequestDispatcher("changepass.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

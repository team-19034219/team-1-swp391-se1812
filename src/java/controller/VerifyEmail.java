/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import model.*;
import dal.StudentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

/**
 *
 * @author nguye
 */
public class VerifyEmail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet VerifyEmail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet VerifyEmail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public static boolean isValidEmail(String email) {
        String emailPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    protected void sendEmail(HttpServletRequest request, HttpServletResponse response, String email)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String verifyemail = "";
        String OTP = "";
        final String username = "nghiahtsdad@gmail.com";
        final String password = "lgcl lzbj lsxv gjuj";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587"); //port 25 465
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        props.put("mail.smtp.starttls.enable", "true");

        Session session2 = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session2);
            message.addHeader("Context-type", "text/html;charset=UTF-8");
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(MimeUtility.encodeText("You want to change your password!", "UTF-8", "B"));

            if (session.getAttribute("verifyemail") != null) {
                verifyemail = (String) session.getAttribute("verifyemail");
            }

            if (session.getAttribute("OTP") != null) {
                OTP = (String) session.getAttribute("OTP");
            }

            String htmlContent = "<div style=\" margin-left: 36%; padding: 10px; width: 400px; border: solid 1px gainsboro; border-radius: 10px;\">"
                    + "<div style=\"margin-left: 150px;\"><img src=\"https://i.postimg.cc/50fJXxTQ/logo2.jpg\" style=\"width: 100px;\"></div>\n"
                    + "<div><p style=\"font-weight: bold; font-size: 20px; text-align: center;\">Verify email request</p></div>\n"
                    + "<div><p style=\"text-align: center;\">" + verifyemail + "</p></div>\n"
                    + "<div><p>We noticed a new request to verify to your Google Account on a Windows device."
                    + "This is your OTP to countinous register in Flearn:</p></div>\n"
                    + "<div><p style=\"font-size:50px; text-align: center;\">"+OTP+"</p></div>\n"
                    + "</div>";

            message.setContent(htmlContent, "text/html;charset=UTF-8");

            Transport.send(message);

//            System.out.println("Email sent successfully.");
        } catch (MessagingException e) {
            throw new ServletException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        String validMessage;
        String email;
        StudentDAO dao = new StudentDAO();
        List<Students> studentList = dao.getAllStudent();

        try {
            email = (String) request.getParameter("email");

            for (Students student : studentList) {
                if (!student.getEmail().isBlank()) {
                    if (email.equals(student.getEmail())) {
                        validMessage = "Your emails is exist. Please enter again!";
                        request.setAttribute("error", validMessage);
                        throw new NumberFormatException();
                    }
                }
            }

            if (!email.isBlank()) {
                if (isValidEmail(email) == false) {
                    validMessage = "Your email not follow format abc@gmail.com. Please enter again!";
                    request.setAttribute("error", validMessage);
                    throw new NumberFormatException();
                }
            }

            String otp = OTPGenerator.generateOTP();

            session.setAttribute("OTP", otp);
            session.setAttribute("verifyemail", email);
            session.setAttribute("otpCreationTime", new Date());
            session.setMaxInactiveInterval(3 * 60);
            sendEmail(request, response, email);
            response.sendRedirect("logup");

        } catch (NumberFormatException e) {
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.StudentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import model.OTPGenerator;
import model.Students;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author FPT SHOP
 */
public class SignUpControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignUpControl</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignUpControl at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private boolean isValidPassword(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        }
        String emailRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(password).matches();
    }

    private boolean isValidPhoneNumber(String phoneNumber) {
        // Biểu thức chính quy để kiểm tra định dạng số điện thoại
        String phoneRegex = "^(0|\\+84)(3[2-9]|5[689]|7[06-9]|8[1-689]|9[0-46-9])\\d{7}$";

        // Kiểm tra số điện thoại có khớp với biểu thức chính quy không
        return phoneNumber.matches(phoneRegex);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("OTP") != null) {
            request.setAttribute("sucess", "Please check your email to get the OTP(3 minute) to register!");
        }
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String validMessage;
        String service;
        String email = "";
        String otp = "";
        String cfotp;
        String phoneNumber;
        String username;
        String password;
        String cfpassword;
        Date otpCreationTime = new Date();
        StudentDAO dao = new StudentDAO();
        List<Students> studentList = dao.getAllStudent();
        try {
            service = request.getParameter("service");

            if (session.getAttribute("verifyemail") != null) {
                email = (String) session.getAttribute("verifyemail");
            }
            
            if (session.getAttribute("OTP") != null) {
                otp = (String) session.getAttribute("OTP");
            }

            cfotp = (String) request.getParameter("cfotp");
            phoneNumber = (String) request.getParameter("phonenumber");
            username = (String) request.getParameter("username");
            password = (String) request.getParameter("password");
            cfpassword = (String) request.getParameter("cfpassword");

            if (session.getAttribute("otpCreationTime") != null) {
                otpCreationTime = (Date) session.getAttribute("otpCreationTime");
            }

            long currentTime = new Date().getTime();
            long otpTime = otpCreationTime.getTime();
            long timeDiff = currentTime - otpTime;

            if (!otp.equals(cfotp)) {
                validMessage = "The OTP you enter not same in your email. Please try again!";
                request.setAttribute("error3", validMessage);
                throw new NumberFormatException();
            }

            if (timeDiff >= 180000) {
                validMessage = "Your otp code has expired. Please click on button verify email again!";
                request.setAttribute("error3", validMessage);
                throw new NumberFormatException();
            }

            if (!isValidPhoneNumber(phoneNumber)) {
                validMessage = "Your phone is not valid. Please enter again!";
                request.setAttribute("error2", validMessage);
                throw new NumberFormatException();
            }

            for (int i = 0; i < studentList.size(); i++) {
                if (studentList.get(i).getUsername().equals(username)) {
                    validMessage = "The username has been exists, Please change your username!";
                    request.setAttribute("error1", validMessage);
                    throw new NumberFormatException();
                }
            }

            if (!password.equals(cfpassword)) {
                validMessage = "The password and confirmed password need to be at the same!";
                request.setAttribute("error1", validMessage);
                throw new NumberFormatException();
            }

            if (!isValidPassword(password)) {
                validMessage = "Password must contain at least 1 uppercase letter, 1 lowercase letter, 1 numeric character, and a minimum length of 6!";
                request.setAttribute("error1", validMessage);
                throw new NumberFormatException();
            }

            if (service == null) {
                validMessage = "Please click on term of service to register!";
                request.setAttribute("error1", validMessage);
                throw new NumberFormatException();
            }

            if (email.isBlank() || username.isBlank() || password.isBlank() || cfpassword.isBlank()) {
                validMessage = "Please enter all infomation and click on term of service to register!";
                request.setAttribute("error1", validMessage);
                throw new NumberFormatException();
            }

            String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
            dao.insertStudent(username, hashed, email, phoneNumber);
            session.removeAttribute("OTP");
            session.removeAttribute("verifyemail");
            response.sendRedirect("login");

        } catch (NumberFormatException e) {
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CategoryOfCourse;
import model.Courses;

public class CourseDAO extends DBContext {

    private CategoryDAO cd = new CategoryDAO();

    // Retrieve all courses from the Course table
    public List<Courses> getAllCourse() {
        List<Courses> list = new ArrayList<>();
        String sql = "SELECT * FROM Course";

        try (Connection conn = connection; PreparedStatement st = conn.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                Courses course = new Courses(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getDouble("price"),
                        rs.getDate("date"),
                        rs.getInt("CategoryOfCourse_ID"),
                        rs.getInt("Lecturer_ID"),
                        rs.getFloat("discount"),
                        rs.getString("img"),
                        rs.getString("video"),
                        rs.getString("description"),
                        rs.getString("time_of_course"),
                        rs.getInt("status"));
                list.add(course);
            }
        } catch (SQLException e) {
            System.err.println("SQL Exception: " + e.getMessage());
        } finally {
            // Close the connection here
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    System.err.println("Error closing connection: " + ex.getMessage());
                }
            }
        }

        return list;
    }

}

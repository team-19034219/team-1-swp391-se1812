<%-- 
    Document   : viewcategory
    Created on : May 15, 2024, 8:22:10 AM
    Author     : nduya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="main.css" />
        <style>
            .CategoryCh{
                margin-top: 20px;
                margin-left: 40px;
            }
            .btn {
                margin-bottom: 5px;
            }

            .grid {
                position: relative;
                width: 100%;
                background: #fff;
                color: #666666;
                border-radius: 2px;
                margin-bottom: 25px;
                box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
            }

            .grid .grid-body {
                padding: 15px 20px 15px 20px;
                font-size: 0.9em;
                line-height: 1.9em;
            }

            .search table tr td.rate {
                color: #f39c12;
                line-height: 50px;
            }

            .search table tr:hover {
                cursor: pointer;
            }

            .search table tr td.image {
                width: 50px;
            }

            .search table tr td img {
                width: 50px;
                height: 50px;
            }

            .search table tr td.rate {
                color: #f39c12;
                line-height: 50px;
            }

            .search table tr td.price {
                font-size: 1.5em;
                line-height: 50px;
            }

            .search #price1,
            .search #price2 {
                display: inline;
                font-weight: 600;
            }

            .short-range {
                width: 70%;
            }
        </style>
    </head>
    <body>
        <!--header area -->
        <jsp:include page="header.jsp"/>
        <!--header end -->

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <div class="container">
            <div class="row">
                <!-- BEGIN SEARCH RESULT -->
                <div class="col-md-12">
                    <div class="grid search">
                        <div class="grid-body">
                            <div class="row">
                                <!-- BEGIN FILTERS -->
                                <div class="col-md-3">
                                    <h2 class="grid-title"><i class="fa fa-filter"></i> Filters</h2>
                                    <hr>

                                    <form id="myForm" action="viewcategory" method="post">
                                        <!-- BEGIN FILTER BY CATEGORY -->
                                        <h4>By category:</h4>
                                        <c:forEach var="category" items="${requestScope.listcategory}" varStatus="status">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="cid_refinee" id="c${status.index}" value="${category.get(i).getId()}" ${chid[i+1]?"checked":""} class="icheck" onclick="setCheck(this)"}> ${category.name}</label>
                                            </div>  
                                        </c:forEach>

                                        <!-- END FILTER BY CATEGORY -->

                                        <div class="padding"></div>



                                        <div class="padding"></div>
                                        <h4>By rate:</h4>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck">   
                                                <i class="fa fa-star"></i>
                                        </div>




                                        <!-- BEGIN FILTER BY PRICE -->
                                        <h4>By price:</h4>
                                        Between <div id="price1">${sprice}</div> to <div id="price2">${eprice}</div>
                                        <input type="number" id="priceFrom" name="pricefrom" value="${sprice}" min="${minprice}" max="${maxprice}">
                                        <input type="number" id="priceTo" name="priceto" value="${eprice}" min="${minprice}" max="${maxprice}">
                                        <!-- END FILTER BY PRICE -->

                                        <h4>By Discount:</h4>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck"> 50%</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck"> 30%</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" class="icheck"> 20%</label>
                                        </div>

                                        <!-- Button -->
                                        <button class="submit-price" style="">Apply</button>
                                    </form>
                                </div>
                                <!-- END FILTERS -->
                                <!-- BEGIN RESULT -->
                                <div class="col-md-9">
                                    <hr>
                                    <!-- BEGIN SEARCH INPUT -->
                                    <form action="searchCourse" method="post">
                                        <div class="input-group" style="width: 80%">
                                            <input value="${txtSearch}" name="txt" type="text" class="form-control" placeholder="Search Course" style="border-radius: 10px; margin-right: 10px">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </form>
                                    <!-- END SEARCH INPUT -->
                                    <c:if test="${txtSearch != null}">
                                        <p>Showing all results matching "${txtSearch}"</p>
                                    </c:if>

                                    <div class="padding"></div>

                                    <div class="row">
                                        <!-- BEGIN ORDER RESULT -->
                                        <div class="col-sm-6">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    Order by <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Name</a></li>
                                                    <li><a href="#">Date</a></li>
                                                    <li><a href="#">View</a></li>
                                                    <li><a href="#">Rating</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- END ORDER RESULT -->


                                    </div>

                                    <!-- BEGIN TABLE RESULT -->
                                    <div class="table-responsive">
                                        <c:forEach var="lcourse" items="${requestScope.listcourse}">
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td class="image"><img src="images/c.jpg" alt=""></td>
                                                        <td class="product" style="width: 500px"><strong>${lcourse.name}</strong><br>${lcourse.description}</td>
                                                        <td class="rate text-right"><span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span></td>
                                                        <td class="price text-right">${lcourse.price}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </c:forEach>
                                    </div>
                                    <!-- END TABLE RESULT -->

                                    <!-- BEGIN PAGINATION -->
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </nav>
                                    <!-- END PAGINATION -->
                                </div>
                                <!-- END RESULT -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SEARCH RESULT -->
            </div>
        </div>
        <!--footer area start-->
        <jsp:include page="footer.jsp"/>
        <!--footer area end-->

        <!-- all js here -->
        <script>
            function updatePriceRange(value) {
                document.getElementById('price2').innerText = value;
                document.getElementById('priceTo').value = value;
            }

            function setCheck(obj) {
                var checkboxes = document.getElementsByName('cid_refinee');
                if (obj.id === 'c0' && obj.checked) {
                    for (var i = 1; i < checkboxes.length; i++) {
                        checkboxes[i].checked = false;
                    }
                } else {
                    for (var i = 1; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxes[0].checked = false;
                            break;
                        }
                    }
                }
                document.getElementById('myForm').submit();
            }
        </script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
            crossorigin="anonymous"
        ></script>

    </body>
</html>
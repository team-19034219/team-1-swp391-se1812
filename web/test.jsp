<%-- 
    Document   : test
    Created on : May 23, 2024, 9:19:02 AM
    Author     : nguye
--%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="user" value="${alo}" />
        <c:out value="${user.getId()}" /><br>
        <c:out value="${user.isVerified_email()}" /><br>
        <c:out value="${user.getName()}" /><br>
    </body>
</html>

USE [master]
GO

/*******************************************************************************
   Drop database if it exists
********************************************************************************/
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'SWP1')
BEGIN
	ALTER DATABASE SWP1 SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE SWP1 SET ONLINE;
	DROP DATABASE SWP1;
END

GO

CREATE DATABASE SWP1
GO

USE SWP1
GO

/*******************************************************************************
	Drop tables if exists
*******************************************************************************/
DECLARE @sql nvarchar(MAX) 
SET @sql = N'' 

SELECT @sql = @sql + N'ALTER TABLE ' + QUOTENAME(KCU1.TABLE_SCHEMA) 
    + N'.' + QUOTENAME(KCU1.TABLE_NAME) 
    + N' DROP CONSTRAINT ' -- + QUOTENAME(rc.CONSTRAINT_SCHEMA)  + N'.'  -- not in MS-SQL
    + QUOTENAME(rc.CONSTRAINT_NAME) + N'; ' + CHAR(13) + CHAR(10) 
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
    ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
    AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
    AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

EXECUTE(@sql) 

GO
DECLARE @sql2 NVARCHAR(max)=''

SELECT @sql2 += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
FROM   INFORMATION_SCHEMA.TABLES
WHERE  TABLE_TYPE = 'BASE TABLE'

Exec Sp_executesql @sql2 
GO 

---------- CREATE TABLE -----------------------------

CREATE TABLE Roles(
	id int primary key not null,
	name nvarchar(20)
);

CREATE TABLE Accounts (
	id INT identity(1,1) PRIMARY KEY NOT NULL,
    username NVARCHAR(20),
    password NVARCHAR(20) NOT NULL,
    Role_ID int,
	FOREIGN KEY (Role_ID) REFERENCES Roles(id)
);

CREATE TABLE Admins (
	id int identity(1,1) PRIMARY KEY NOT NULL,
	Account_ID INT,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
    FOREIGN KEY (Account_ID) REFERENCES Accounts(id)
);

CREATE TABLE Users(
	id int identity(1,1) PRIMARY KEY NOT NULL,
    Account_ID INT,
	fullName NVARCHAR(50),
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
    FOREIGN KEY (Account_ID) REFERENCES Accounts(id)
);

CREATE TABLE Instructors (
	id int identity(1,1) PRIMARY KEY NOT NULL,
	Account_ID INT,
	fullName NVARCHAR(50),
	age int,
    gender int,
	address NVARCHAR(20),
	email NVARCHAR(50),
	phone_number NVARCHAR(20),
    birthday DATE,
    FOREIGN KEY (Account_ID) REFERENCES Accounts(id)
);



CREATE TABLE Category (
    id INT identity(1,1) PRIMARY KEY,
    name NVARCHAR(50), 
);

CREATE TABLE Course(
    id int identity(1,1) PRIMARY KEY NOT NULL,
	name NVARCHAR(50) NOT NULL,
	price FLOAT,
    date Date NOT NULL,
	Category_ID INT,
	Instructor_ID INT,
	discount float,
	img nvarchar(100),
	video nvarchar(50),
	description nvarchar(500),
    FOREIGN KEY (Category_ID) REFERENCES Category(id),
	FOREIGN KEY (Instructor_ID) REFERENCES Instructors(id)
);

CREATE TABLE Orders(
	id int identity(1,1) PRIMARY KEY,
	Users_ID int,
	Course_ID int,
	date varchar(40),
	description nvarchar(100),
	number_Of_Item int,
	total float,
	status int,
	FOREIGN KEY (Users_ID) REFERENCES Users(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
);


CREATE TABLE Payments(
	id int identity(1,1) PRIMARY KEY,
	Course_ID int,
	Order_ID int,
	type nvarchar(50),
	content_payment nvarchar(50),
	quantity int,
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Order_ID) REFERENCES Orders(id),
	
);

CREATE TABLE Feedback(
	rate int,
	content nvarchar(300),
	daycomment date,
	Users_ID int,
	Course_ID int,
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Users_ID) REFERENCES Users(id)
);

CREATE TABLE Blog(
	id int identity(1,1) PRIMARY KEY NOT NULL,
	Admin_ID int,
	Course_ID int,
	Category_ID int,
	title nvarchar(100),
	content nvarchar(500),
	daypost date,
	tag nvarchar(100),
	number_of_like int
	FOREIGN KEY (Admin_ID) REFERENCES Admins(id),
	FOREIGN KEY (Course_ID) REFERENCES Course(id),
	FOREIGN KEY (Category_ID) REFERENCES Category(id)
);

CREATE TABLE Comments(
	content nvarchar(500),
	Blog_ID int,
	Users_ID int,
	day_comment date,
	number_of_like int,
	FOREIGN KEY (Blog_ID) REFERENCES Blog(id),
	FOREIGN KEY (Users_ID) REFERENCES Users(id)
);
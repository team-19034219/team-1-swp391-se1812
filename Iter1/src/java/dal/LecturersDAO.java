/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Lecturers;


public class LecturersDAO extends DBContext {
    public List<Lecturers> getAllLectures() {
        List<Lecturers> lectures = new ArrayList<>();
        String sql = "SELECT * FROM Lecturers";
        
        try (Connection conn = connection;
             PreparedStatement st = conn.prepareStatement(sql);
             ResultSet rs = st.executeQuery()) {
            
            while (rs.next()) {
                Lecturers lecture = new Lecturers();
                lecture.setId(rs.getInt("id"));
                lecture.setUsername(rs.getString("username"));
                lecture.setPassword(rs.getString("password"));
                lecture.setRole_ID(rs.getInt("role_ID"));
                lecture.setFullName(rs.getString("fullName"));
                lecture.setGender(rs.getInt("gender"));
                lecture.setAddress(rs.getString("address"));
                lecture.setEmail(rs.getString("email"));
                lecture.setPhone_number(rs.getString("phone_number"));
                lecture.setBirthday(rs.getDate("birthday"));
                lecture.setImg_certificates(rs.getString("img_certificates"));
                lecture.setImg_profile(rs.getString("img_profile"));
                lecture.setDescription(rs.getString("description"));
                
                lectures.add(lecture);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        
        return lectures;
    }
}

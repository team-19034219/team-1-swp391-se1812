
package model;
import java.util.Date;

public class Course {
    private int id;
    private String name;
    private double price;
    private Date date;
    private int categoryOfCourseId;
    private int lectureId;
    private float discount;
    private String img;
    private String video;
    private String documentLink;
    private String description;
    private String time_of_course;

    // Constructors
    public Course() {}

    public Course(int id, String name, double price, Date date, int categoryOfCourseId, int lectureId, float discount, String img, String video, String documentLink, String description, String time_of_course) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.date = date;
        this.categoryOfCourseId = categoryOfCourseId;
        this.lectureId = lectureId;
        this.discount = discount;
        this.img = img;
        this.video = video;
        this.documentLink = documentLink;
        this.description = description;
        this.time_of_course = time_of_course;
    }

    public String getTime_of_course() {
        return time_of_course;
    }

    public void setTime_of_course(String time_of_course) {
        this.time_of_course = time_of_course;
    }

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCategoryOfCourseId() {
        return categoryOfCourseId;
    }

    public void setCategoryOfCourseId(int categoryOfCourseId) {
        this.categoryOfCourseId = categoryOfCourseId;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

   public float getDiscount() {
    return discount;
}


    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDocumentLink() {
        return documentLink;
    }

    public void setDocumentLink(String documentLink) {
        this.documentLink = documentLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public double getPriceDiscount(){
        double discountAmount = (int) price* (discount);
        double discountedPrice = price - discountAmount;
        return discountedPrice;
    }
    

    
    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", date=" + date +
                ", categoryOfCourseId=" + categoryOfCourseId +
                ", lectureId=" + lectureId +
                ", discount=" + discount +
                ", img='" + img + '\'' +
                ", video='" + video + '\'' +
                ", documentLink='" + documentLink + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}


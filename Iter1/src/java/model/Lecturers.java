package model;

import java.util.Date;

public class Lecturers {
    private int id;
    private String username;
    private String password;
    private int role_ID;
    private String fullName;
    private int gender;
    private String address;
    private String email;
    private String phone_number;
    private Date birthday;
    private String img_certificates;
    private String img_profile;
    private String description;

    // Constructors
    public Lecturers() {
    }

    public Lecturers(String username, String password, int roleId, String fullName, int gender, String address, String email, String phoneNumber, Date birthday, String imgCertificates, String imgProfile, String description) {
        this.username = username;
        this.password = password;
        this.role_ID = roleId;
        this.fullName = fullName;
        this.gender = gender;
        this.address = address;
        this.email = email;
        this.phone_number = phoneNumber;
        this.birthday = birthday;
        this.img_certificates = imgCertificates;
        this.img_profile = imgProfile;
        this.description = description;
    }

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole_ID() {
        return role_ID;
    }

    public void setRole_ID(int role_ID) {
        this.role_ID = role_ID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getImg_certificates() {
        return img_certificates;
    }

    public void setImg_certificates(String img_certificates) {
        this.img_certificates = img_certificates;
    }

    public String getImg_profile() {
        return img_profile;
    }

    public void setImg_profile(String img_profile) {
        this.img_profile = img_profile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   
    @Override
    public String toString() {
        return "Lecture{" + "id=" + id + ", username=" + username + 
                ", password=" + password + ", roleId=" + role_ID + 
                ", fullName=" + fullName + ", gender=" + gender + ", address=" 
                + address + ", email=" + email + ", phoneNumber=" + phone_number
                + ", birthday=" + birthday + ", imgCertificates=" + img_certificates
                + ", imgProfile=" + img_profile + ", description=" + description + '}';
    }

    
}

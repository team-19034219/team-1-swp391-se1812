package controller;

import dal.CategoryDAO;
import dal.CourseDAO;
import dal.LecturersDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.CategoryOfCourse;
import model.Course;
import model.Lecturers;

public class HomeViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO courseDAO = new CourseDAO();
        LecturersDAO ld = new LecturersDAO();
        CategoryDAO cd = new CategoryDAO();
        // Lấy danh sách các khóa học và giảng viên
        List<Course> courses = courseDAO.getAllCourse();

        List<Lecturers> lectures = ld.getAllLectures();

        List<CategoryOfCourse> cate = cd.getAllCategory();

        // Đặt danh sách vào thuộc tính của yêu cầu
        request.setAttribute("courses", courses);
        request.setAttribute("cate", cate);
        request.setAttribute("lectures", lectures);

        // Chuyển hướng đến trang home.jsp
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
